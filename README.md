# ECHOAnalysis

## Project Introduction 
This repository contains codes for echocardiography analysis, including **segmentation** and **motion tracking**, with specific design of shape prior and motion prior for segmentation and motion estimation in echocardiography for better generalisation. 

## Method overview 
![method](method.png)

*Shape and motion priors for segmentation and motion tracking*

## Citation 
The codes of segmentation is built upon the following paper *Shape constraints in deep learning for robust 2D echocardiography analysis* in FIMH 2021. If you find the repository useful, please cite the following bibtex. 
```
@inproceedings{yang2021shape,
  title={Shape constraints in deep learning for robust 2D echocardiography analysis},
  author={Yang, Yingyu and Sermesant, Maxime},
  booktitle={International Conference on Functional Imaging and Modeling of the Heart},
  pages={22--34},
  year={2021},
  organization={Springer}
}
```

The codes of motion tracking is built upon *Unsupervised Polyaffine Transformation Learning for Echocardiography Motion Estimation* in FIMH 2023. If you find the repository useful, please cite the following bibtex. 
```
@inproceedings{yang2023unsupervised,
  title={Unsupervised Polyaffine Transformation Learning for Echocardiography Motion Estimation},
  author={Yang, Yingyu and Sermesant, Maxime},
  booktitle={International Conference on Functional Imaging and Modeling of the Heart},
  pages={384--393},
  year={2023},
  organization={Springer}
}
@article{siarohin2019first,
  title={First order motion model for image animation},
  author={Siarohin, Aliaksandr and Lathuili{\`e}re, St{\'e}phane and Tulyakov, Sergey and Ricci, Elisa and Sebe, Nicu},
  journal={Advances in neural information processing systems},
  volume={32},
  year={2019}
}

```
