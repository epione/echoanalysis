'''
Codes adapted from https://github.com/AliaksandrSiarohin/first-order-model
'''

import torch
from torch import nn
import torch.nn.functional as F
from modules.dense_motion import DenseMotionNetwork
from modules.keypoint_detector import KPDetector
from modules.losses import NCC, DisLoss, MyoLoss, ImcompressLoss
import numpy as np
from modules.util import AntiAliasInterpolation2d, make_coordinate_grid
from torch.autograd import grad

def deform_heatmap(inp, deformation):
    _, h_old, w_old, _ = deformation.shape
    _, _, h, w = inp.shape
    return F.grid_sample(inp, deformation)


def gaussian2kp(heatmap, device):
    """
    Extract the mean and from a heatmap
    """
    shape = heatmap.shape
    heatmap = heatmap.unsqueeze(-1)
    grid = make_coordinate_grid(shape[2:], heatmap.type()).unsqueeze_(0).unsqueeze_(0).to(device)
    value = (heatmap * grid).sum(dim=(2, 3))

    return value


class Transform:
    """
    Random tps transformation for equivariance constraints. See Sec 3.3
    """
    def __init__(self, bs, **kwargs):
        noise = torch.normal(mean=0, std=kwargs['sigma_affine'] * torch.ones([bs, 2, 3]))
        self.theta = noise + torch.eye(2, 3).view(1, 2, 3)
        self.bs = bs

        if ('sigma_tps' in kwargs) and ('points_tps' in kwargs):
            self.tps = True
            self.control_points = make_coordinate_grid((kwargs['points_tps'], kwargs['points_tps']), type=noise.type())
            self.control_points = self.control_points.unsqueeze(0)
            self.control_params = torch.normal(mean=0,
                                               std=kwargs['sigma_tps'] * torch.ones([bs, 1, kwargs['points_tps'] ** 2]))
        else:
            self.tps = False

    def transform_frame(self, frame):
        grid = make_coordinate_grid(frame.shape[2:], type=frame.type()).unsqueeze(0)
        grid = grid.view(1, frame.shape[2] * frame.shape[3], 2)
        grid = self.warp_coordinates(grid).view(self.bs, frame.shape[2], frame.shape[3], 2)
        return F.grid_sample(frame, grid, padding_mode="reflection")

    def warp_coordinates(self, coordinates):
        theta = self.theta.type(coordinates.type())
        theta = theta.unsqueeze(1)
        transformed = torch.matmul(theta[:, :, :, :2], coordinates.unsqueeze(-1)) + theta[:, :, :, 2:]
        transformed = transformed.squeeze(-1)

        if self.tps:
            control_points = self.control_points.type(coordinates.type())
            control_params = self.control_params.type(coordinates.type())
            distances = coordinates.view(coordinates.shape[0], -1, 1, 2) - control_points.view(1, 1, -1, 2)
            distances = torch.abs(distances).sum(-1)

            result = distances ** 2
            result = result * torch.log(distances + 1e-6)
            result = result * control_params
            result = result.sum(dim=2).view(self.bs, coordinates.shape[1], 1)
            transformed = transformed + result

        return transformed

    def jacobian(self, coordinates):
        new_coordinates = self.warp_coordinates(coordinates)
        grad_x = grad(new_coordinates[..., 0].sum(), coordinates, create_graph=True)
        grad_y = grad(new_coordinates[..., 1].sum(), coordinates, create_graph=True)
        jacobian = torch.cat([grad_x[0].unsqueeze(-2), grad_y[0].unsqueeze(-2)], dim=-2)
        return jacobian


class LossMotion(nn.Module):
    def __init__(self, train_params, common_params, dict_data, device = None,
                 img_loss = 'NCC'):
        super(LossMotion, self).__init__()
        self.loss_weights = train_params['loss_weights']
        self.train_params = train_params
        self.img_sim = img_loss
        self.loss_L2 = torch.nn.MSELoss()
        self.loss_L1 = torch.nn.L1Loss()
        self.ncc_loss = NCC(device)
        self.myo_loss = MyoLoss()
        self.kp_dis_loss = DisLoss(dict_data.astype(np.float32), 0.1, img_size = common_params['img_size'], device=device)
        self.imcompress_loss = ImcompressLoss()
        self.device = device
        self.ref_pts = torch.from_numpy(dict_data.astype(np.float32)).to(device)
        self.num_kp = common_params['num_kp']


    def create_direct_sparse_motions(self, source_image, kp_driving, kp_source, jacobian_driving, jacobian_source):
        """
        f: the position of frame in image sequence, f>=1
        """

        bs, _, h, w = source_image.shape
        identity_grid = make_coordinate_grid((h, w), type=kp_source.type())
        identity_grid = identity_grid.view(1, 1, h, w, 2)
        coordinate_grid = identity_grid - kp_driving.view(bs, self.num_kp, 1, 1, 2)

        ori_jacobian = torch.matmul(jacobian_source, torch.inverse(jacobian_driving))
        jacobian = ori_jacobian.unsqueeze(-3).unsqueeze(-3)
        jacobian = jacobian.repeat(1, 1, h, w, 1, 1)

        coordinate_grid = torch.matmul(jacobian, coordinate_grid.unsqueeze(-1))
        coordinate_grid = coordinate_grid.squeeze(-1)

        driving_to_source = coordinate_grid + kp_source.view(bs, self.num_kp, 1, 1, 2)

        # adding background feature
        identity_grid = identity_grid.repeat(bs, 1, 1, 1, 1)
        sparse_motions = torch.cat([identity_grid, driving_to_source], dim=1)
        return sparse_motions

    def deform_input(self, inp, deformation):
        _, h_old, w_old, _ = deformation.shape
        _, _, h, w = inp.shape
        if h_old != h or w_old != w:
            deformation = deformation.permute(0, 3, 1, 2)
            deformation = F.interpolate(deformation, size=(h, w), mode='bilinear')
            deformation = deformation.permute(0, 2, 3, 1)
        return F.grid_sample(inp, deformation)

    def forward(self, x, kp_pts, generated):
        # kp_pts [B, N, 2], B = T
        generated['value'] = kp_pts['value']
        generated['kp_heatmap'] = kp_pts['heatmap']

        loss_values = {}
        # ==========================================================================================================
        # image deformation loss
        # ==========================================================================================================
        if self.loss_weights['deformed_image'] != 0:
            loss_values['deformed_image'] = self.ncc_loss(x['video'][:-1], generated['deformed']) * \
                                            self.loss_weights['deformed_image']
        # ==========================================================================================================
        # landmark deformation loss
        # ==========================================================================================================
        if self.loss_weights['deformed_hm'] !=0:
            deformed_source_kp = deform_heatmap(kp_pts['heatmap'][1:], generated['deformation'])
            deformed_source_flatten = deformed_source_kp.view(deformed_source_kp.shape[0],
                                                              deformed_source_kp.shape[1], -1)
            deformed_sum = deformed_source_flatten.sum(dim=-1).unsqueeze(-1)
            deformed_source_norm = deformed_source_flatten / (deformed_sum + 1e-6)
            deformed_source_norm = deformed_source_norm.view(deformed_source_kp.shape)
            deformed_source_value = gaussian2kp(deformed_source_norm, self.device)
            generated['prediction_kp'] = deformed_source_value
            loss_deformed_hm = torch.abs(kp_pts['value'][:-1] - deformed_source_value).mean()
            loss_values['deformed_hm'] = loss_deformed_hm * self.loss_weights['deformed_hm']

        # ==========================================================================================================
        # l2 norm loss
        # ==========================================================================================================
        if self.loss_weights['l2_loss'] != 0:
            ot_tmp = self.kp_dis_loss(kp_pts['value'][x['ED']:x['ED']+1])
            loss_values['l2_loss'] = ot_tmp * self.loss_weights['l2_loss']

        # ==========================================================================================================
        # kp myo loss
        # ==========================================================================================================
        if self.loss_weights['myo_loss'] !=0:
            myo_ed_loss = self.myo_loss(generated['kp_heatmap'][x['ED']:x['ED']+1], x['ED_MYO'])
            myo_es_loss = self.myo_loss(generated['kp_heatmap'][x['ES']:x['ES']+1], x['ES_MYO'])
            loss_values['myo_loss'] = (myo_es_loss + myo_ed_loss) * self.loss_weights['myo_loss']

        # ==========================================================================================================
        # equivariance loss
        # ==========================================================================================================
        if (self.loss_weights['equivariance_value'] + self.loss_weights['equivariance_jacobian']) != 0:
            transform = generated['transform']
            transformed_frame = generated['transformed_frame']
            transformed_kp = generated['transformed_kp']

            ## Value loss part
            if self.loss_weights['equivariance_value'] != 0:
                value = torch.abs(kp_pts['value'][:-1] - transform.warp_coordinates(transformed_kp['value'])).mean()
                loss_values['equivariance_value'] = self.loss_weights['equivariance_value'] * value

            ## jacobian loss part
            if self.loss_weights['equivariance_jacobian'] != 0:
                jacobian_transformed = torch.matmul(transform.jacobian(transformed_kp['value']),
                                                    transformed_kp['jacobian'])

                normed_driving = torch.inverse(kp_pts['jacobian'][:-1])
                normed_transformed = jacobian_transformed
                value = torch.matmul(normed_driving, normed_transformed)
                eye = torch.eye(2).view(1, 1, 2, 2).type(value.type())
                value = torch.abs(eye - value).mean()
                loss_values['equivariance_jacobian'] = self.loss_weights['equivariance_jacobian'] * value

        # =====================================================================================
        # ED-all other frame transformation (direct motion)
        # =====================================================================================
        # deform others to ed
        if self.loss_weights['deformed_image_rest2ed'] != 0:
            num_f = len(x['video'])
            es_sparse_motion = self.create_direct_sparse_motions(x['video'],
                                                                 kp_pts['value'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1),
                                                                 kp_pts['value'],
                                                                 kp_pts['jacobian'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1, 1),
                                                                 kp_pts['jacobian']
                                                                 )
            es_sparse_motion = es_sparse_motion.permute(0, 1, 4, 2, 3)
            es_deformation = (es_sparse_motion * generated['mask'].unsqueeze(2)[x['ED']:x['ED'] + 1]).sum(dim=1)
            es_deformation = es_deformation.permute(0, 2, 3, 1)
            generated['deformation_rest2ed'] = es_deformation
            deformed_es = self.deform_input(x['video'], es_deformation)
            generated['deformed_rest2ed'] = deformed_es
            # image deformation loss
            loss_values['deformed_image_rest2ed'] = self.ncc_loss(x['video'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1, 1), deformed_es) * \
                                               self.loss_weights['deformed_image_rest2ed']
            # landmark deformation loss
            if self.loss_weights['deformed_hm_rest2ed'] != 0:
                deformed_source_kp = deform_heatmap(kp_pts['heatmap'],
                                                    es_deformation)
                deformed_source_flatten = deformed_source_kp.view(deformed_source_kp.shape[0],
                                                                  deformed_source_kp.shape[1], -1)
                deformed_sum = deformed_source_flatten.sum(dim=-1).unsqueeze(-1)
                deformed_source_norm = deformed_source_flatten / (deformed_sum + 1e-6)
                deformed_source_norm = deformed_source_norm.view(deformed_source_kp.shape)
                deformed_source_value = gaussian2kp(deformed_source_norm, self.device)
                generated['prediction_kp_rest2ed'] = deformed_source_value
                loss_deformed_hm = torch.abs(kp_pts['value'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1) - deformed_source_value).mean()
                loss_values['deformed_hm_rest2ed'] = loss_deformed_hm * self.loss_weights['deformed_hm_rest2ed']

        # deform ED to others
        if self.loss_weights['deformed_image_ed2rest'] != 0:
            num_f = len(x['video'])
            ed_sparse_motion = self.create_direct_sparse_motions(x['video'][x['ED']:x['ED']+1].repeat(num_f, 1, 1, 1),
                                                                 kp_pts['value'],
                                                                 kp_pts['value'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1),
                                                                 kp_pts['jacobian'],
                                                                 kp_pts['jacobian'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1, 1)
                                                                 )
            ed_sparse_motion = ed_sparse_motion.permute(0, 1, 4, 2, 3)
            ed_deformation = (ed_sparse_motion * generated['mask'].unsqueeze(2)).sum(dim=1)
            ed_deformation = ed_deformation.permute(0, 2, 3, 1)
            generated['deformation_ed2rest'] = ed_deformation
            deformed_ed = self.deform_input(x['video'][x['ED']:x['ED']+1].repeat(num_f, 1, 1, 1), ed_deformation)
            generated['deformed_ed2rest'] = deformed_ed
            # image deformation loss
            loss_values['deformed_image_ed2rest'] = self.ncc_loss(x['video'],
                                                             deformed_ed) * \
                                               self.loss_weights['deformed_image_ed2rest']
            # landmark deformation loss
            if self.loss_weights['deformed_hm_ed2rest'] != 0:
                deformed_source_kp = deform_heatmap(kp_pts['heatmap'][x['ED']:x['ED'] + 1].repeat(num_f, 1, 1, 1),
                                                    ed_deformation)
                deformed_source_flatten = deformed_source_kp.view(deformed_source_kp.shape[0],
                                                                  deformed_source_kp.shape[1], -1)
                deformed_sum = deformed_source_flatten.sum(dim=-1).unsqueeze(-1)
                deformed_source_norm = deformed_source_flatten / (deformed_sum + 1e-6)
                deformed_source_norm = deformed_source_norm.view(deformed_source_kp.shape)
                deformed_source_value = gaussian2kp(deformed_source_norm, self.device)
                generated['prediction_kp_ed2rest'] = deformed_source_value
                loss_deformed_hm = torch.abs(
                    kp_pts['value'] - deformed_source_value).mean()
                loss_values['deformed_hm_ed2rest'] = loss_deformed_hm * self.loss_weights['deformed_hm_ed2rest']

        # ==========================================================================================================
        # Imcompressibility loss
        # ==========================================================================================================
        if self.loss_weights['imcompress_loss'] !=0:
            imcompress = self.imcompress_loss(kp_pts['jacobian'])
            loss_values['imcompress_loss'] = imcompress * self.loss_weights['imcompress_loss']
        return loss_values, generated


class SequenceMotion(nn.Module):
    def __init__(self, train_params, common_params, kp_detector_params, dense_motion_params, device = None):
        super(SequenceMotion, self).__init__()
        self.kp_extractor = KPDetector(**kp_detector_params, **common_params)
        self.motion_estimator = DenseMotionNetwork(**dense_motion_params, **common_params, device=device)
        self.train_params = train_params

    def forward(self, x):
        # video: ED, +1, +2, ... ES, +1, +2, ...
        # kp_pts [B, N, 2], B = T
        kp_pts = self.kp_extractor(x['video'])
        # =====================================================================================
        # consecutive frame transformation
        # =====================================================================================
        generated = self.motion_estimator(source_image=x['video'][1:], all_kpts=kp_pts['value'],
                                          kp_driving=kp_pts['value'][:-1],
                                          kp_source=kp_pts['value'][1:],
                                          jacobian_driving=kp_pts['jacobian'][:-1],
                                          jacobian_source=kp_pts['jacobian'][1:],
                                          preheatmap=kp_pts['pre_heatmap'][:-1])

        transform = Transform(x['video'].shape[0] - 1, **self.train_params['transform_params'])
        transformed_frame = transform.transform_frame(x['video'][:-1])
        transformed_kp = self.kp_extractor(transformed_frame)

        generated['transformed_frame'] = transformed_frame
        generated['transformed_kp'] = transformed_kp
        generated['transform'] = transform
        return kp_pts, generated




