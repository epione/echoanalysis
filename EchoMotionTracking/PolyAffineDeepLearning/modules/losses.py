'''
Codes adapted from https://github.com/AliaksandrSiarohin/first-order-model
'''

import torch
import torch.nn.functional as F
import numpy as np
import math

class ImcompressLoss(torch.nn.Module):
    def __init__(self):
        super(ImcompressLoss, self).__init__()
    def forward(self, jacobian):
        # jacobian (T,K,2,2)
        det = jacobian[:, :, 0, 0] * jacobian[:, :, 1, 1] - jacobian[:, :, 0, 1] * jacobian[:, :, 1, 0]
        return ((det-1)**2).mean()


def pairwise_distances_sq_l2(x, y):
    x_norm = (x ** 2).sum(1).view(-1, 1)
    y_t = torch.transpose(y, 0, 1)
    y_norm = (y ** 2).sum(1).view(1, -1)
    dist = x_norm + y_norm - 2.0 * torch.mm(x, y_t)
    return torch.clamp(dist, 1e-5, 1e5) / x.size(1)


def pairwise_distances_cos(x, y):
    b = x.size(0)
    x_norm = torch.sqrt((x ** 2).sum(2).view(b, -1, 1))
    y_t = torch.transpose(y, 1, 2)
    y_norm = torch.sqrt((y ** 2).sum(2).view(b, 1, -1))
    dist = 1. - torch.bmm(x, y_t) / x_norm / y_norm
    return dist


class DisLoss(torch.nn.Module):
    def __init__(self, ref_pts, sigma, img_size = 128, device=None):
        super(DisLoss, self).__init__()
        self.ref_pts = torch.from_numpy(ref_pts).to(device)*2/img_size - 1
        self.sigma = torch.ones(len(ref_pts)).to(device) * sigma
        self.device = device

    def forward(self, new_pts):
        b, n = new_pts.size(0), new_pts.size(1)
        ref_pts = self.ref_pts.unsqueeze(0).repeat(b, 1, 1)
        sigma = self.sigma.unsqueeze(1).repeat(1, 2)
        out = torch.mean((new_pts - ref_pts)**2, dim=0) / sigma
        return out.sum()/n


class MyoLoss(torch.nn.Module):
    def __init__(self):
        super(MyoLoss, self).__init__()

    def forward(self, heatmap, myomap):
        '''
        myomap: (1, 1, h, w)
        heatmap: (1, K, h, w)
        '''
        _, _, h_old, w_old = heatmap.size()
        _, _, h, w = myomap.size()
        if h_old != h:
            heatmap = F.interpolate(heatmap, size=(h, w), mode='bilinear')
        myomap = - (myomap - 0.5) * 2
        out = torch.sum(heatmap * myomap, dim=(2, 3)).mean()
        return out


class REMD(torch.nn.Module):
    """
    Relaxed earth mover distance loss
    """
    def __init__(self, device=None, cos_d=True, splits= [32], lambd_value=0.025):
        super(REMD, self).__init__()
        self.cos_d = cos_d
        self.splits = splits
        self.lambd_value = lambd_value
        self.device = device

    def get_DMat(self, X, Y):
        M = 0
        cb = 0
        for i in range(len(self.splits)):
            if self.cos_d:
                ce = cb + self.splits[i]
                M = M + pairwise_distances_cos(X[:, :, cb:ce], Y[:, :, cb:ce])
                cb = ce
            else:
                ce = cb + self.splits[i]
                M = M + torch.sqrt(pairwise_distances_sq_l2(X[:, :, cb:ce], Y[:, :, cb:ce]))
                cb = ce
        return M

    def forward(self, X, Y):
        b, N = X.size(0), X.size(1)
        M = Y.size(1)
        pairwise_diff_x = (X.view(b, N, 1, 2) - X.view(b, 1, N, 2))
        pairwise_diff_y = (Y.view(b, M, 1, 2) - Y.view(b, 1, M, 2))
        CX_M_x = self.get_DMat(pairwise_diff_x[:, :, :, 0], pairwise_diff_y[:, :, :, 0])
        CX_M_y = self.get_DMat(pairwise_diff_x[:, :, :, 1], pairwise_diff_y[:, :, :, 1])

        CX_M = CX_M_x + CX_M_y
        CX_M_EYE = CX_M * torch.tile(torch.eye(min(self.splits[0], CX_M.size(1))), (b, 1, 1)).float().to(self.device)

        eye1 = torch.sum(CX_M_EYE, 1)
        eye2 = torch.sum(CX_M_EYE, 2)
        eye_m1, _ = CX_M.min(1)
        eye_m2, _ = CX_M.min(2)
        remd_mean = torch.max(eye_m1.mean(), eye_m2.mean())
        remd_eye = torch.max((eye1 - eye_m1).mean(), (eye2 - eye_m2).mean()) * 4.0

        return remd_mean + remd_eye


class NCC(torch.nn.Module):
    """
    Local (over window) normalized cross correlation loss.
    """

    def __init__(self, device, win=None):
        super(NCC, self).__init__()
        self.win = win
        self.device = device

    def forward(self, y_true, y_pred):

        Ii = y_true
        Ji = y_pred

        # get dimension of volume
        # assumes Ii, Ji are sized [batch_size, *vol_shape, nb_feats]
        ndims = len(list(Ii.size())) - 2
        assert ndims in [1, 2, 3], "volumes should be 1 to 3 dimensions. found: %d" % ndims

        # set window size
        win = [9] * ndims if self.win is None else self.win

        # compute filters
        sum_filt = torch.ones([1, 1, *win]).to(self.device)

        pad_no = math.floor(win[0] / 2)

        if ndims == 1:
            stride = (1)
            padding = (pad_no)
        elif ndims == 2:
            stride = (1, 1)
            padding = (pad_no, pad_no)
        else:
            stride = (1, 1, 1)
            padding = (pad_no, pad_no, pad_no)

        # get convolution function
        conv_fn = getattr(F, 'conv%dd' % ndims)

        # compute CC squares
        I2 = Ii * Ii
        J2 = Ji * Ji
        IJ = Ii * Ji

        I_sum = conv_fn(Ii, sum_filt, stride=stride, padding=padding)
        J_sum = conv_fn(Ji, sum_filt, stride=stride, padding=padding)
        I2_sum = conv_fn(I2, sum_filt, stride=stride, padding=padding)
        J2_sum = conv_fn(J2, sum_filt, stride=stride, padding=padding)
        IJ_sum = conv_fn(IJ, sum_filt, stride=stride, padding=padding)

        win_size = np.prod(win)
        u_I = I_sum / win_size
        u_J = J_sum / win_size

        cross = IJ_sum - u_J * I_sum - u_I * J_sum + u_I * u_J * win_size
        I_var = I2_sum - 2 * u_I * I_sum + u_I * u_I * win_size
        J_var = J2_sum - 2 * u_J * J_sum + u_J * u_J * win_size

        cc = cross * cross / (I_var * J_var + 1e-5)

        return - torch.mean(cc)