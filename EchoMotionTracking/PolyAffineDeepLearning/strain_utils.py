'''
Author: Yingyu Yang
'''

import numpy as np
from skimage.measure import label
from skimage.morphology import binary_opening, binary_closing, square, dilation
from skimage import transform, measure
import os

import matplotlib.pyplot as plt
from matplotlib import animation
from sklearn.cluster import KMeans

from sklearn.cluster import KMeans
from skimage.morphology.convex_hull import convex_hull_image

# drift compensation
def drift_comp(y):
    out = y - y * np.arange(len(y)) / (len(y) - 1)
    return out

def line_func(pt1, pt2):
    # ax + by + c = 0
    y1, x1 = pt1
    y2, x2 = pt2
    ty = y1-y2
    tx = x1-x2
    if tx==0:
        b = 0
        c = 1
        a = -1/x1 if x1!=0 else -1/x2
    else:
        b = -1
        a = ty/tx
        c = - a * x1 - b * y1
    return a,b,c


def get_ortho_func(a,b,c,pt):
    y,x = pt
    newa, newb, newc = a,b,c
    if a!=0:
        newb=-1
        newa = b/a
    else:
        newa = -1
        newb = a/b
    newc = -newa * x - newb * y
    return newa, newb, newc


def get_aha7(ed_mask4):
    # cut myo into 7 regions
    # 1. get apex, basal pts
    myoed = ed_mask4[2]
    xx, yy = np.meshgrid(np.arange(128), np.arange(128))
    basal, apex = get_apex_basal(ed_mask4)
    mid_basal = np.mean(basal, axis=0)
    cutpts = np.array([np.linspace(apex[0], mid_basal[0], 4),
                       np.linspace(apex[1], mid_basal[1], 4)]).T
    # cutpts = cutpts[np.array([1, 4, 7, 10])]
    # 2. cut the myo into 7 regions
    ca, cb, cc = line_func(apex, mid_basal)
    leftm = ca * xx + cb * yy + cc <= 0
    rightm = ca * xx + cb * yy + cc >= 0
    # orthogonal line
    orthos = [[0, 0, -1]]
    for pt in cutpts[:-1]:
        ta, tb, tc = get_ortho_func(ca, cb, cc, pt)
        orthos.append([ta, tb, tc])
    orthos.append([0, 0, 1])
    bands = []
    for k in range(len(orthos) - 1):
        a1, b1, c1 = orthos[k]
        a2, b2, c2 = orthos[k + 1]
        tmp = (a1 * xx + b1 * yy + c1 <= 0) * (a2 * xx + b2 * yy + c2 >= 0)
        bands.append(tmp)
    ahas = []
    for i in range(3):
        ahas.append(bands[3 - i] * leftm * myoed)
    for j in range(3):
        ahas.append(bands[j + 1] * rightm * myoed)
    ahas.append(bands[0] * myoed)
    #aha7 = np.sum(np.array([ahas[k] * (k + 1) for k in range(len(ahas))]), axis=0)
    aha7 = np.zeros((128,128))
    k = 1
    for aha in ahas:
        idy, idx = np.where(aha==1)
        aha7[idy, idx] = k
        k+=1
    return ahas, aha7


class AHA_strain:
    def __init__(self, n_cluster, img_size, myo, lv=None, resol=[1., 1.]):
        self.n_cluster = n_cluster
        self.img_size = img_size
        self.myo = myo.astype(int)
        self.resol = resol #y,x
        if lv is None:
            self.lv = binary_opening(convex_hull_image(self.myo)^self.myo, square(5))
        else:
            self.lv = lv

        self.mask4 = np.zeros((4, 128, 128))
        self.mask4[1] = self.lv
        self.mask4[2] = self.myo
        self.grid = np.stack(np.meshgrid(np.arange(128), np.arange(128)), axis=0)
        self.kmeans_myo_regions()
        #self.hard_myo_regions()
        self.aha_regional_direction()

    def hard_myo_regions(self):
        self.n_cluster = 7
        ahas, aha7 = get_aha7(self.mask4)
        self.pts = np.array(np.where(self.myo == 1)).T
        centers = []
        for i in range(self.n_cluster):
            aha = ahas[i]
            idy, idx = np.where(aha==1)
            centers.append([np.mean(idy), np.mean(idx)])
        self.centers = np.array(centers)
        self.mask_knns = ahas
        self.mask_knn = aha7

    def kmeans_myo_regions(self):
        pts = np.array(np.where(self.myo == 1)).T #(y,x)
        knn = KMeans(n_clusters=self.n_cluster, n_init=10)
        knn.fit(pts)
        pts_labels = knn.labels_
        kcenters = knn.cluster_centers_

        # order of kclusters
        heart_center = np.mean(pts, axis=0)
        heart_center[0] = self.img_size - 8
        thetas = []
        for kc in kcenters:
            v = heart_center - kc
            theta = np.arccos(np.sum((v) * np.array([0, 1])) / np.linalg.norm(v))
            thetas.append(theta)
        order = np.argsort(thetas)

        # re-order centers
        new_labels = []
        for i in range(len(pts_labels)):
            tmp = np.where(order == pts_labels[i])[0]
            new_labels.append(tmp)

        mask_knn14 = np.zeros((self.img_size, self.img_size))
        mask_knn14s = np.zeros((self.n_cluster, self.img_size, self.img_size))
        for k in range(len(pts)):
            pt = pts[k]
            nn = new_labels[k]
            # np.where(order==pts_labels[k])[0]
            mask_knn14[pt[0], pt[1]] = nn + 1
            mask_knn14s[nn, pt[0], pt[1]] = 1
        self.pts = pts
        self.centers = np.array([kcenters[i] for i in order])
        self.labels = new_labels
        self.mask_knn = mask_knn14
        self.mask_knns = mask_knn14s

    def aha_regional_direction(self):
        laplace_grid, rad_myo, long_myo = get_projection_mat(self.myo, self.lv)
        self.radial_dir = rad_myo
        self.longitudinal_dir = long_myo
        aha_dir_rad = []
        aha_dir_long = []
        for i in range(self.n_cluster):
            pts = np.where(self.mask_knn == i + 1)
            # normalize
            rad_ = np.mean(rad_myo[:, pts[0], pts[1]], axis=1)
            rad_ = rad_ /np.linalg.norm(rad_)
            aha_dir_rad.append(rad_)
            long_ = np.mean(long_myo[:, pts[0], pts[1]], axis=1)
            long_ = long_/np.linalg.norm(long_)
            if long_[1] > 0:
                aha_dir_long.append(-long_)
            else:
                aha_dir_long.append(long_)
        aha_dir_rad = np.array(aha_dir_rad)
        aha_dir_long = np.array(aha_dir_long)
        self.aha_radial_dir = aha_dir_rad
        self.aha_longitudinal_dir = aha_dir_long

    def calculate_aha_strain(self, pos):
        # one frame position matrix (2, h, w) (x,y)
        oriE = get_E_from_pos(pos, self.myo)
        # project E according to AHA regional directions
        rad_strain = project_E_direction_aha(oriE, self.aha_radial_dir, self.mask_knn, self.n_cluster)
        long_strain = project_E_direction_aha(oriE, self.aha_longitudinal_dir, self.mask_knn, self.n_cluster)
        return oriE, rad_strain, long_strain

    def calculate_aha_displacement(self, pos):
        dis = pos - self.grid
        pos_dis = np.sqrt((dis[0] * self.resol[1]) ** 2 + (dis[1] * self.resol[0]) ** 2)
        aha_dis = []
        aha_dis7 = 0
        for aha in self.mask_knns:
            idx = np.where(aha == 1)
            aha_dis.append(np.mean(pos_dis[idx[0], idx[1]]))
            aha_dis7 += aha * np.mean(pos_dis[idx[0], idx[1]])
        # normalized aha dis (https://arxiv.org/pdf/2111.05790.pdf)
        # [0,7], [1,6], [2,5]
        pairs = [[0,7], [1,6], [2,5]]
        # get interval measurement of each frame
        # Manhattan distance between the middle of opposite segments
        norminterv = np.zeros(8)
        for p1,p2 in pairs:
            idx1 = np.where(self.mask_knns[p1]==1)
            idx2 = np.where(self.mask_knns[p2]==1)
            pos1 = np.mean(pos[:, idx1[0], idx1[1]], axis=1) * (np.array(self.resol)[::-1])
            pos2 = np.mean(pos[:, idx2[0], idx2[1]], axis=1) * (np.array(self.resol)[::-1])
            interv = np.sum(np.abs(pos1-pos2))
            norminterv[p1] = interv
            norminterv[p2] = interv
        return aha_dis, aha_dis7, norminterv

    def calculate_dense_strain(self, pos):
        # one frame position matrix (2, h, w) (x,y)
        oriE = get_E_from_pos(pos, self.myo)
        # project E according to pixel directions
        rad_strain = project_E_direction(oriE, self.radial_dir, self.myo)
        long_strain = project_E_direction(oriE, self.longitudinal_dir, self.myo)
        return oriE, rad_strain, long_strain


def project_E_direction_aha(emat, hmat, myo14, nregion = 14):
    # emat: the strain tensor (h,w,2,2)
    # hmat: the direction to project (n,2)
    h,w,_,_ = emat.shape
    ahaE = []
    for i in range(nregion):
        idy, idx = np.where(myo14==i+1)
        curE = []
        for y,x in zip(idy, idx):
            curE.append(hmat[i:i+1] @ emat[y,x] @ hmat[i:i+1].T)
        ahaE.append(np.mean(np.array(curE)))
    return ahaE


def project_E_direction(emat, hmat, myo):
    # emat: the strain tensor (y,x,2,2)
    # hmat: the direction to project (2,y,x)
    h,w,_,_ = emat.shape
    idy, idx = np.where(myo)
    outE = np.zeros((h,w))
    for y,x in zip(idy, idx):
        outE[y,x] = hmat[:,y,x] @ emat[y,x] @ hmat[:,y,x].T
    return outE


def get_E_from_pos(pos, myo, resol=[1,1]):
    # pos shape (2, h, w) # displacement vector (x,y)
    _, y, x = pos.shape
    pos = np.moveaxis(pos, 0, -1)
    resol = np.array(resol) #(x,y)
    posx1 = pos - np.roll(pos, axis=1, shift=1)
    posx2 = np.roll(pos, axis=1, shift=-1) - pos
    posy1 = pos - np.roll(pos, axis=0, shift=1)
    posy2 = np.roll(pos, axis=0, shift=-1) - pos
    outm = np.zeros((y, x, 2, 2))
    # [[dx/dX, dx/dY],[dy/dX, dy/dY]]
    outm[:, :, :, 0] = (posx1 + posx2)/2/resol[0]
    outm[:, :, :, 1] = (posy1 + posy2)/2/resol[1]
    idy, idx = np.where(myo)
    outE = np.zeros_like(outm)
    for y, x in zip(idy, idx):
        f = outm[y,x]
        e = 0.5 * (f.T @ f - np.identity(2))
        outE[y, x] = e
    return outE


from matplotlib.collections import LineCollection
def plot_grid(x,y, ax=None, **kwargs):
    ax = ax or plt.gca()
    segs1 = np.stack((x,y), axis=2)
    segs2 = segs1.transpose(1,0,2)
    ax.add_collection(LineCollection(segs1, **kwargs))
    ax.add_collection(LineCollection(segs2, **kwargs))
    ax.autoscale()


# save sequence gif
def anim_sequence(img_sq, pts=None, kpts=None, heatmaps=None, myo=None, save_folder='', name='', s=1.5):
    fig = plt.figure(figsize=(8, 8))
    plt.tight_layout()
    ax1 = fig.add_subplot(111)

    if pts is not None:
        idy, idx = np.where(myo)

    def init():
        ax1.imshow(img_sq[0], cmap='gray')
        if pts is not None:
            ax1.scatter(pts[0, 0, idy, idx], pts[0, 1, idy, idx], s=s)
        if kpts is not None:
            ax1.scatter(kpts[0,:, 0], kpts[0,:,1], s=85, color='tab:orange')
        ax1.axis('off')
    def animate(i):
        ax1.clear()
        ax1.imshow(img_sq[i], cmap='gray')
        if pts is not None:
            ax1.scatter(pts[i, 0, idy, idx], pts[i, 1, idy, idx], s=s)
        if kpts is not None:
            ax1.scatter(kpts[i,:, 0], kpts[i,:,1], s=85, color='tab:orange')
    num_slice = len(img_sq)
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=num_slice, interval=100, blit=False)
    Writer = animation.writers['pillow']
    writer = Writer(fps=20, bitrate=1800)
    anim.save(os.path.join(save_folder, name + '_img_sq.gif'), writer=writer)
    plt.close()


def getLargestCC(segmentation):
    # first filling hole and get rid of small points
    segmentation = binary_closing(segmentation, square(5))
    # then find the largest component
    labels = label(segmentation)
    assert (labels.max() != 0)  # assume at least 1 CC
    largestCC = labels == np.argmax(np.bincount(labels.flat)[1:]) + 1
    return largestCC


def get_apex_basal(mask):
    # when there is no intersection
    flag = False
    lv = mask[1]
    la = mask[3]
    try:
        lv = getLargestCC(lv)
        la = getLargestCC(la)
    except:
        pass

    try:
        lv = dilation(lv, square(3))
        la = dilation(la, square(3))
        coord = np.where(lv * la)
        rm_idx = np.argmax(coord[1])
        lm_idx = np.argmin(coord[1])
        basal = np.zeros((2, 2))
        # (y,x)
        basal[0] = np.array([coord[0][lm_idx], coord[1][lm_idx]])
        basal[1] = np.array([coord[0][rm_idx], coord[1][rm_idx]])
        mid_basal = np.mean(basal, axis=0)
    except:
        # reshape myo & lv
        myo, lv = modify_post_myo(mask[2])
        mask[2] = myo.copy()
        mask[1] = lv.copy()
        img = dilation(lv, square(3)) * dilation(myo, square(3))
        tmp = dilation(lv, square(3)).astype(float) - lv.astype(float) - img.astype(float)
        #basal = find_endpoints(img)
        coord = np.where(tmp==1)
        rm_idx = np.argmax(coord[1])
        lm_idx = np.argmin(coord[1])
        basal = np.zeros((2, 2))
        # (y,x)
        basal[0] = np.array([coord[0][lm_idx], coord[1][lm_idx]])
        basal[1] = np.array([coord[0][rm_idx], coord[1][rm_idx]])
        mid_basal = np.mean(basal, axis=0)
        flag = True
    myo_con = measure.find_contours(mask[1], 0.5)[0]
    dis = 0
    idx = -1
    count = 0
    for pt in myo_con:
        new_dis = np.linalg.norm(pt - mid_basal)
        if new_dis > dis:
            dis = new_dis
            idx = count
        count += 1
    apex = myo_con[idx]
    return basal, apex


def line_tan(p1, p2):
    y1, x1 = p1
    y2, x2 = p2
    a = (y2 - y1) / (y1 * x2 - y2 * x1)
    b = (x1 - x2) / (y1 * x2 - y2 * x1)
    return a, b


def get_aha6_regions_new(mask):
    # (y,x)
    basal, apex_endo = get_apex_basal(mask)
    cp = (basal[0] + basal[1]) / 2
    a, b = line_tan(cp, apex_endo)
    apex = apex_endo.copy()
    for i in range(0, 256):
        if mask[2][i, int((-1 - b * i) / a)] == 1:
            apex[0] = i
            apex[1] = int((-1 - b * i) / a)
            break
    step_y = np.abs(apex[0] - cp[0]) / 3
    pts = []
    ort_abs = []
    for i in range(3):
        y = apex[0] + i * step_y
        x = (-1 - b * y) / a if a != 0 else 0
        pts.append([y, x])
        ort_abs.append([b, -a, a * y - b * x])
    ort_abs = np.array(ort_abs)
    pts = np.array(pts)
    mask_myo = mask[2]
    _, m, n = mask.shape
    xx, yy = np.meshgrid(np.arange(m), np.arange(n))
    subs = []

    sub_left = a * xx + b * yy + 1 <= 0
    sub_right = a * xx + b * yy + 1 >= 0

    # region 1+2
    sub_mask1 = ort_abs[0, 0] * xx + ort_abs[0, 1] * yy + ort_abs[0, 2] >= 0
    sub_mask2 = ort_abs[1, 0] * xx + ort_abs[1, 1] * yy + ort_abs[1, 2] <= 0
    sub1 = sub_mask1 * sub_mask2 * mask_myo
    sub11 = sub1 * sub_left
    sub12 = sub1 * sub_right
    subs.append(sub11)
    subs.append(sub12)
    # region 3+4
    sub_mask1 = ort_abs[1, 0] * xx + ort_abs[1, 1] * yy + ort_abs[1, 2] >= 0
    sub_mask2 = ort_abs[2, 0] * xx + ort_abs[2, 1] * yy + ort_abs[2, 2] <= 0
    sub2 = sub_mask1 * sub_mask2 * mask_myo
    sub21 = sub2 * sub_left
    sub22 = sub2 * sub_right
    subs.append(sub21)
    subs.append(sub22)
    # region 5+6
    sub3 = ort_abs[2, 0] * xx + ort_abs[2, 1] * yy + ort_abs[2, 2] >= 0
    sub3 = sub3 * mask_myo
    sub31 = sub3 * sub_left
    sub32 = sub3 * sub_right
    subs.append(sub31)
    subs.append(sub32)
    return subs


# calculate gradient
def gradient(R):
    # gradient = R(x+1)-R(x)
    if R.ndim==2:
        dx = R-np.roll(R,1,axis=1)
        dy = R-np.roll(R,1,axis=0)
        return np.stack([dx,dy],axis=0)
    else:
        # jacobian
        dxx = R[0]-np.roll(R[0],1,axis=1)
        dyx = R[1]-np.roll(R[1],1,axis=1)
        dxy = R[0]-np.roll(R[0],1,axis=0)
        dyy = R[1]-np.roll(R[1],1,axis=0)
        grad = np.stack([dxx,dyx,dxy,dyy],axis=0)
        return grad.reshape((2,2,grad.shape[1],grad.shape[2]))


from skimage.morphology import binary_opening, binary_closing, square, dilation, closing
def get_projection_mat(myo, lv):
    epi = binary_closing(myo + lv, square(3)).astype(bool)
    endo = lv.astype(bool)
    wall = np.copy(epi)
    wall[endo] = False

    wall_idx_i, wall_idx_j = np.argwhere(wall).T

    hi = hj = 1
    hi2 = hi ** 2
    hj2 = hj ** 2

    n_points = len(wall_idx_i)

    max_iterations = 50
    iteration = 0
    factor = 1/4

    laplace_grid = np.zeros(endo.shape, float)
    laplace_grid[~epi] = 1

    while iteration < max_iterations:
        iteration += 1
        for n in range(n_points):
            i = wall_idx_i[n]
            j = wall_idx_j[n]
            prev_value = laplace_grid[i, j]
            value = (
                    (laplace_grid[i + 1, j] + laplace_grid[i - 1, j]) / hi2
                    + (laplace_grid[i, j + 1] + laplace_grid[i, j - 1]) / hj2
                    ) * factor
            laplace_grid[i, j] = value
    tang_myo = gradient(laplace_grid) * myo
    ortho_myo = np.copy(tang_myo)
    idx,idy = np.where(myo)
    for x,y in zip(idx, idy):
        cur_tang = tang_myo[:, x,y]
        ortho_myo[:, x,y] = np.array([-cur_tang[1], cur_tang[0]])
    #tang_myo[1] = -tang_myo[1]
    #ortho_myo[1] = -ortho_myo[1]
    return laplace_grid, tang_myo, ortho_myo


def get_aha6_centers(mask_ed, aha6):
    _,h,w =  mask_ed.shape
    id_grid = np.stack(np.meshgrid(np.arange(w), np.arange(h)))
    vent = (mask_ed[1] + mask_ed[2]).astype(bool)
    idxy = np.where(vent)
    cent_x = np.mean(id_grid[0,idxy[0],idxy[1]])
    cent_y = np.mean(id_grid[1,idxy[0],idxy[1]])
    # get centers
    cent_regions = []
    for i in range(6):
        region = aha6[i]
        idx_region = np.where(region>0)
        mean_x = np.mean(id_grid[0,idx_region[0],idx_region[1]])
        mean_y = np.mean(id_grid[1,idx_region[0],idx_region[1]])
        cent_regions.append(np.array([mean_x,mean_y]))
    cent_regions = np.array(cent_regions)
    return cent_regions


def get_aha6_directions(tang_myo, aha6):
     # get regional tangents and normal
    mat_regions = []
    for i in range(6):
        region = aha6[i]
        idx_region = np.where(region>0)
        mean_x = np.mean(tang_myo[0,idx_region[0],idx_region[1]])
        # TIP: remember the reverse direction of y-axis
        mean_y = -np.mean(tang_myo[1,idx_region[0],idx_region[1]])
        tang = -np.array([mean_x,mean_y])/np.sqrt(mean_x**2+mean_y**2)
        ortho = np.array([-mean_y/mean_x,1])/np.sqrt(1+(mean_y/mean_x)**2)
        mat = np.stack([tang, ortho])
        mat_regions.append(mat)
    mat_regions = np.array(mat_regions)
    return mat_regions


def get_E_from_dis(dis, myo):
    # dis shape (2,y,x)
    dis = np.moveaxis(dis, 0, -1)
    disx1 = dis - np.roll(dis, axis=1, shift=1)
    disx2 = np.roll(dis, axis=1, shift=-1) - dis
    disy1 = dis - np.roll(dis, axis=0, shift=1)
    disy2 = np.roll(dis, axis=0, shift=-1) - dis
    s = dis.shape # (y,x,2)
    outm = np.zeros((s[0], s[1], s[2], 2))
    outm[:, :, :, 0] = (disx1+disx2)/2
    outm[:, :, :, 1] = (disy1+disy2)/2
    idx, idy = np.where(myo)
    outE = np.zeros(outm.shape)
    for x, y in zip(idx, idy):
        f = outm[x, y]
        e = 0.5*(f.T + f + f.T@f)
        outE[x, y] = e
    return outE


def project_aha6_regional_strain(e_mat_list,aha6, mat_regions, mean=True):
    regional_proj_E = []
    for i in range(6):
        region = aha6[i]
        idx_region = np.where(region>0)
        mat = mat_regions[i]
        seq_regione = []
        for f in range(len(e_mat_list)):
            regione = []
            e_mat = e_mat_list[f]
            for i,j in zip(idx_region[0], idx_region[1]):
                emat = e_mat[i,j]
                # here mat is (e11, e12)
                #             (e21, e22)
                # the mat is already the transposed Q.T
                regione.append(mat@emat@mat.T)
            if mean:
                seq_regione.append(sum(regione)/len(regione))
            else:
                seq_regione.append(np.array(regione))
        regional_proj_E.append(seq_regione)
    regional_proj_E = [np.stack(e) for e in regional_proj_E]
    return regional_proj_E


from skimage.morphology.convex_hull import convex_hull_image
def modify_post_myo(myo):
    myo = myo.astype(int)
    lv = binary_opening(convex_hull_image(myo)^myo, square(5))
    return np.array([myo,lv])