'''
Codes adapted from https://github.com/AliaksandrSiarohin/first-order-model
'''

import matplotlib
matplotlib.use('Agg')
import os, sys
from tqdm import trange
import yaml
from argparse import ArgumentParser
from shutil import copy
import numpy as np
from modules.seq_motion import SequenceMotion, LossMotion
import torch
from torch.utils.data import DataLoader
from logger import Logger
from torch.optim.lr_scheduler import MultiStepLR
import random
from dataset import EchoEchonetDataset, split_echonet


torch.manual_seed(0)
torch.cuda.manual_seed_all(0)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(0)
random.seed(0)


if __name__ == "__main__":
    torch.autograd.set_detect_anomaly(True)
    if sys.version_info[0] < 3:
        raise Exception("You must use Python 3 or higher. Recommended version is Python 3.7")

    parser = ArgumentParser()
    parser.add_argument("--config", required=True, help="path to config")
    parser.add_argument("--log_dir", default='log', help="path to log into")
    parser.add_argument("--checkpoint", default=None, help="path to checkpoint to restore")
    parser.add_argument("--name", required=True, help="path to model")

    opt = parser.parse_args()
    with open(opt.config) as f:
        config = yaml.load(f)

    if opt.checkpoint is not None:
        log_dir = os.path.join(*os.path.split(opt.checkpoint)[:-1])
    else:
        log_dir = os.path.join(opt.log_dir, os.path.basename(opt.config).split('.')[0])
        log_dir += '_' + opt.name

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    if not os.path.exists(os.path.join(log_dir, os.path.basename(opt.config))):
        copy(opt.config, log_dir)

    # ===============================================================
    # Modify here information about EchoNet dataset
    data_csv = ''
    data_folder = ''
    dict_file = ''
    # ===============================================================

    checkpoint = opt.checkpoint
    dict_data = np.load(dict_file)
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    train_params = config['train_params']

    # set models
    generator = SequenceMotion(config['train_params'],
                               config['model_params']['common_params'],
                               config['model_params']['kp_detector_params'],
                               config['model_params']['dense_motion_params'],
                               device = device)
    generator.to(device)
    loss_generator = LossMotion(config['train_params'], config['model_params']['common_params'],
                                dict_data, device = device, img_loss = 'NCC')
    loss_generator.to(device)

    # prepare datasets
    train_files, valid_files, test_files = split_echonet(data_csv)
    dataset = EchoEchonetDataset(train_files, data_folder, is_train=True, imgsize=128, transform=None, maxlength=50)
    dataset_valid = EchoEchonetDataset(valid_files, data_folder, is_train=True, imgsize=128, transform=None, maxlength=50)
    dataloader = DataLoader(dataset, batch_size=train_params['batch_size'], shuffle=True, num_workers=3, drop_last=True)
    dataloader_valid = DataLoader(dataset_valid, batch_size=1, shuffle=True, num_workers=3, drop_last=True)

    # optimizer setting
    optimizer_generator = torch.optim.Adam(generator.parameters(), lr=train_params['lr_generator'], betas=(0.5, 0.999))
    if checkpoint is not None:
        start_epoch = Logger.load_cpk(checkpoint, generator)
    else:
        start_epoch = 0
    scheduler_generator = MultiStepLR(optimizer_generator, train_params['epoch_milestones'], gamma=0.1,
                                     last_epoch= - 1)
    if start_epoch>0:
        for i in range(start_epoch):
            scheduler_generator.step()

    # start training
    with Logger(log_dir=log_dir, visualizer_params=config['visualizer_params'],
                checkpoint_freq=train_params['checkpoint_freq']) as logger:
        for epoch in trange(start_epoch, train_params['num_epochs']):
            for x in dataloader:
                x['video'] = x['video'][0].to(device, dtype=torch.float)
                x['ED_MYO'] = x['ED_MYO'].to(device, dtype=torch.float)
                x['ES_MYO'] = x['ES_MYO'].to(device, dtype=torch.float)
                kp_pts, generated = generator(x)
                losses_generator, generated = loss_generator(x, kp_pts, generated)

                loss_values = [val.mean() for val in losses_generator.values()]
                loss = sum(loss_values)

                loss.backward()
                optimizer_generator.step()
                optimizer_generator.zero_grad()

                losses = {key: value.mean().detach().data.cpu().numpy() for key, value in losses_generator.items()}
                logger.log_iter(losses=losses)

            scheduler_generator.step()

            logger.log_epoch(epoch, {'generator': generator}, inp=x, out=generated)

            generator.eval()
            for x in dataloader_valid:
                x['video'] = x['video'][0].to(device, dtype=torch.float)
                x['ED_MYO'] = x['ED_MYO'].to(device, dtype=torch.float)
                x['ES_MYO'] = x['ES_MYO'].to(device, dtype=torch.float)
                kp_pts, generated = generator(x)
                losses_generator, generated = loss_generator(x, kp_pts, generated)
                losses = {key: value.mean().detach().data.cpu().numpy() for key, value in losses_generator.items()}
                logger.log_iter(losses=losses)
                optimizer_generator.zero_grad()
            logger.log_scores(list(losses.keys()), train=False)
            if logger.early_stop:
                break