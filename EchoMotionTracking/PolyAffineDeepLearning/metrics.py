import torch
import numpy as np
import os
import torch.nn as nn
import torch.nn.functional as F
from scipy.linalg import expm
from scipy.ndimage import morphology
from time import time


def to_numpy(x):
    if not (isinstance(x, np.ndarray) or x is None):
        if x.is_cuda:
            x = x.data.cpu()
        x = x.detach().numpy()
    return x


def surfd(input1, input2, sampling=1, connectivity=1):
    input_1 = np.atleast_1d(input1.astype(np.bool))
    input_2 = np.atleast_1d(input2.astype(np.bool))

    conn = morphology.generate_binary_structure(input_1.ndim, connectivity)

    S = input_1 ^ morphology.binary_erosion(input_1, conn)
    Sprime = input_2 ^ morphology.binary_erosion(input_2, conn)

    # ~s is the negative of s
    dta = morphology.distance_transform_edt(~S, sampling)
    dtb = morphology.distance_transform_edt(~Sprime, sampling)

    sds = np.concatenate([np.ravel(dta[Sprime != 0]), np.ravel(dtb[S != 0])])

    return sds


def metrics(mask_, gt_, sampling=1):
    lnot = np.logical_not
    land = np.logical_and

    true_positive = np.sum(land((mask_), (gt_)))
    false_positive = np.sum(land((mask_), lnot(gt_)))
    false_negative = np.sum(land(lnot(mask_), (gt_)))
    true_negative = np.sum(land(lnot(mask_), lnot(gt_)))

    M = np.array([[true_negative, false_negative],
                  [false_positive, true_positive]]).astype(np.float64)
    metrics = {}
    metrics['Sensitivity'] = M[1, 1] / (M[0, 1] + M[1, 1] + 1e-5)
    metrics['Specificity'] = M[0, 0] / (M[0, 0] + M[1, 0] + 1e-5)
    metrics['Dice'] = 2 * M[1, 1] / (M[1, 1] * 2 + M[1, 0] + M[0, 1] + 1e-5)

    surf_dis = surfd(mask_, gt_, sampling)
    # add hausdorff distance metric
    if len(surf_dis) == 0:
        metrics['Hausdorff'] = 0
        metrics['Mean Distance Error'] = 0
    else:
        metrics['Hausdorff'] = surf_dis.max()
        # add mean distance metric
        metrics['Mean Distance Error'] = surf_dis.mean()
    return np.array([metrics['Dice'], metrics['Hausdorff'], metrics['Mean Distance Error']])


def evalAllmetric(mask_, gt_, sampling=1):
    num_region = len(gt_)
    cur_metrics = []
    for num in range(1, int(num_region)):
        cur_metrics.append(metrics(mask_[num], gt_[num], sampling))
    cur_metrics = np.concatenate(cur_metrics)
    return cur_metrics


def grad_spatial_temporal(array):
    # array 3d : (t,h,w)
    dx = np.roll(array, axis=-1, shift=-1) - array
    dy = np.roll(array, axis=-2, shift=-1) - array
    spa_dis = np.sqrt(dx ** 2 + dy ** 2)
    temp_dis = array[:-1] - array[1:]
    return np.mean(spa_dis), np.mean(temp_dis)


def det_jac(array):
    # (2,h,w)
    # (x,y)
    dx = np.roll(array, axis=-1, shift=-1) - array
    dy = np.roll(array, axis=-2, shift=-1) - array
    pos_jac = (1 + dx[0]) * (1 + dy[1]) - dx[1] * dy[0]
    return pos_jac


def gradient_det_jac(array):
    dx = np.roll(array, axis=-1, shift=-1) - array
    dy = np.roll(array, axis=-2, shift=-1) - array
    dis = np.sqrt(dx ** 2 + dy ** 2)
    return dis


def soft_dice_loss(y_true, y_pred, epsilon=1e-10):
    '''
    Soft dice loss calculation for arbitrary batch size, number of classes, and number of spatial dimensions.
    Assumes the `channels_last` format.

    # Arguments
        y_true: b x X x Y( x Z...) x c One hot encoding of ground truth
        y_pred: b x X x Y( x Z...) x c Network output, must sum to 1 over c channel (such as after softmax)
        epsilon: Used for numerical stability to avoid divide by zero errors

    # References
        V-Net: Fully Convolutional Neural Networks for Volumetric Medical Image Segmentation
        https://arxiv.org/abs/1606.04797
        More details on Dice loss formulation
        https://mediatum.ub.tum.de/doc/1395260/1395260.pdf (page 72)

        Adapted from https://github.com/Lasagne/Recipes/issues/99#issuecomment-347775022
    '''

    # skip the batch and class axis for calculating Dice score
    axes = tuple(range(1, len(y_pred.shape) - 1))
    numerator = 2. * np.sum(y_pred * y_true, axes)
    denominator = np.sum(np.square(y_pred) + np.square(y_true), axes)

    return 1 - np.mean((numerator + epsilon) / (denominator + epsilon))