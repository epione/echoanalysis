import numpy as np
import matplotlib.pyplot as plt
import SimpleITK as sitk
import os
import torch
from metrics import metrics, grad_spatial_temporal, det_jac
import pandas as pd
import numpy as np
from skimage.transform import resize
from tqdm import tqdm
from modules.seq_motion import SequenceMotion, LossMotion
import yaml
from dataset import EchoEchonetDataset, split_echonet
from torch.utils.data import DataLoader
from modules.dense_motion_diffeom import VecInt
import scipy
from modules.util import make_coordinate_grid



import imageio
import shutil
from matplotlib import animation
# save sequence gif
def save_sq_pts_gif(img_sq, pt_sq, mask, save_folder, name):
    num_slice = len(pt_sq)
    tmp = save_folder + '/' + name
    #print(tmp)
    os.makedirs(tmp, exist_ok=True)
    #print('Begin extract images')
    for f in range(num_slice):
        fig = plt.figure(figsize=(6, 6))
        ax1 = fig.add_subplot(111)
        ax1.imshow(img_sq[f], cmap='gray')
        if mask is not None:
            ax1.imshow(mask[f], alpha=0.4)
        ax1.scatter(pt_sq[f][:, 0], pt_sq[f][:, 1], s=50, label='Motion Estimation')
        ax1.axis('off')
        plt.legend()
        plt.savefig(os.path.join(tmp, '{}.png'.format(f)),
                    transparent=False,
                    facecolor='white'
                    )
        plt.close()
    frames = []
    for t in range(num_slice):
        image = imageio.imread(os.path.join(tmp, '{}.png'.format(t)))
        frames.append(image)
    imageio.mimsave(os.path.join(save_folder, name + '_img_sq.gif'),  # output gif
                    frames,  # array of input frames
                    fps=20)
    shutil.rmtree(tmp)


def to_var(x, device):
    if isinstance(x, np.ndarray):
        x = torch.from_numpy(x)
    x = x.to(device, dtype=torch.float)
    return x


def to_numpy(x):
    if not (isinstance(x, np.ndarray) or x is None):
        if x.is_cuda:
            x = x.data.cpu()
        x = x.detach().numpy()
    return x


def generate_velocity_based_deformation(kp_pts, generated, generator, x, device, ed, es):
    # turn ES to ED, target: ED, source: ES
    source_kpts = kp_pts['value'][es:es+1].unsqueeze(-1).detach().cpu().numpy()
    target_kpts = kp_pts['value'][ed:ed+1].unsqueeze(-1).detach().cpu().numpy()
    linear_map, _ = generator.motion_estimator.create_sparse_motions(x['video'][es:es+1], to_var(target_kpts, device),
                                                                     to_var(source_kpts, device),
                                                                     kp_pts['jacobian'][ed:ed+1],
                                                                     kp_pts['jacobian'][es:es+1])
    linear_map = linear_map.detach().cpu().numpy()
    translation = source_kpts - np.matmul(linear_map, target_kpts)
    homo_mats = np.concatenate(
        [np.concatenate([linear_map, translation], axis=3), np.tile(np.array([[[[0, 0, 1]]]]), (len(source_kpts), 10, 1, 1))], axis=2)
    homo_logmats = []
    for i in range(len(homo_mats)):
        ttlog = []
        for j in range(10):
            ttlog.append(scipy.linalg.logm(homo_mats[i, j]))
        homo_logmats.append(ttlog)
    homo_logmats = np.array(homo_logmats)
    source_image = x['video'][es:es+1]
    bs, _, h, w = source_image.shape
    logmat = to_var(homo_logmats, device)[:, :, :2]
    identity_grid = make_coordinate_grid((h, w), type=logmat.type())  # (x,y)
    identity_grid = identity_grid.view(1, 1, h, w, 2)
    logmat = logmat.unsqueeze(-3).unsqueeze(-3)
    logmat = logmat.repeat(1, 1, h, w, 1, 1)
    logaffine = logmat[:, :, :, :, :, :2]
    logtranslate = logmat[:, :, :, :, :, 2:]
    sparse_velocity = torch.matmul(logaffine, identity_grid.unsqueeze(-1)) + logtranslate
    sparse_motions = torch.cat([to_var(torch.zeros(len(source_image), 1, 128, 128, 2), device), sparse_velocity.squeeze(-1)], dim=1)  #
    weights = generated['mask'][ed:ed+1].unsqueeze(2)
    polyaffine_v = (sparse_motions.permute(0, 1, 4, 2, 3) * weights).sum(dim=1).permute(0, 2, 3, 1)
    return polyaffine_v


if __name__ == "__main__":
    mode = 'nef'
    #nums = [37,15,37,50]
    #for cc in [0]:
    if mode == 'nef':
        model_path = '/data/epione/user/yyang/Echo_Motion_Seg/save/motion_sequence/FOMM/SEQ/echonet-128-01_ECHONET_trainALL9_01'
        dict_data = np.load('/data/epione/user/yyang/Echo_Motion_Seg/EchoNet-Dynamic/kpts128.npy')
        data_csv = '/data/epione/user/yyang/Echo_Motion_Seg/EchoNet-Dynamic/FileList.csv'
        data_folder = '/data/epione/user/yyang/Echo_Motion_Seg/EchoNet-Dynamic/Videos_lv_npy'
    else:
        model_path = '/user/yyang/home/09Echo_Seq_Analysis/save/motion_models_202210/FOMM/SEQ/echonet-128-01_ECHONET_trainALL9_01'
        dict_data = np.load('/user/yyang/home/02ECHO_Project/data/EchoNet-Dynamic/kpts128.npy')
        data_csv = '/user/yyang/home/02ECHO_Project/data/EchoNet-Dynamic/FileList.csv'
        data_folder = '/user/yyang/home/02ECHO_Project/data/EchoNet-Dynamic/Videos_lv_npy'

    config_file = os.path.join(model_path, 'echonet-128-01.yaml')
    with open(config_file) as f:
        config = yaml.load(f)
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    img_size = 128
    generator = SequenceMotion(config['train_params'],
                               config['model_params']['common_params'],
                               config['model_params']['kp_detector_params'],
                               config['model_params']['dense_motion_params'], device=device)
    generator.to(device)
    loss_generator = LossMotion(config['train_params'], config['model_params']['common_params'],
                                dict_data, device=device, img_loss='NCC')
    loss_generator.to(device)
    exp_layer = VecInt((128, 128), 7).to(device)

    checkpoint_path = model_path + '/00000037-checkpoint.pth.tar'
    checkpoint = torch.load(checkpoint_path, device)
    generator.load_state_dict(checkpoint['generator'])
    example_save_folder = os.path.join(model_path, 'echonet_examples')
    os.makedirs(example_save_folder, exist_ok=True)


    train_files, valid_files, test_files = split_echonet(data_csv)
    dataset_test = EchoEchonetDataset(valid_files, data_folder, is_train=True, imgsize=128, transform=None)
    dataloader_test = DataLoader(dataset_test, batch_size=1, shuffle=False,
                                 num_workers=6, drop_last=True)

    es2ed_all = []
    generator.eval()
    loss_generator.eval()
    # only use 100 samples
    for x in tqdm(dataloader_test):
        x['video'] = x['video'][0].to(device, dtype=torch.float)
        x['ED_LV'] = x['ED_LV'].to(device, dtype=torch.float)
        x['ES_LV'] = x['ES_LV'].to(device, dtype=torch.float)
        x['ED_MYO'] = x['ED_MYO'].to(device, dtype=torch.float)
        x['ES_MYO'] = x['ES_MYO'].to(device, dtype=torch.float)
        resol = x['resol']
        kp_pts, generated = generator(x)
        losses_generator, generated = loss_generator(x, kp_pts, generated)
        # es-->ed mask velocity based results
        #poly_velocity = generate_velocity_based_deformation(kp_pts, generated, generator, x, device, x['ED'], x['ES'])
        #es_deformation = exp_layer(poly_velocity)

        # euclidean results
        es_deformation = generated['deformation_rest2ed'][x['ES']:x['ES']+1]

        deformed_es = loss_generator.deform_input(x['ES_LV'], es_deformation)
        m = metrics(deformed_es[0,0].detach().cpu().numpy(),
                    x['ED_LV'][0,0].detach().cpu().numpy(), sampling=resol)
        es2ed_all.append(m)

        '''
        img_sq = generated['deformed_ed2rest'].detach().cpu().numpy()[:, 0]
        pt_sq = (generated['prediction_kp_ed2rest'].detach().cpu().numpy() +1 ) * 64
        #save_sq_pts_gif(img_sq, pt_sq, None, example_save_folder, 'ed2rest05')
        affine_params = kp_pts['affine_single_params'].detach().cpu().numpy()
        plt.figure(figsize=(12,6))
        for i in range(5):
            plt.subplot(2,3,i+1)
            for k in range(10):
                plt.plot(affine_params[:, k, i])
        #plt.show()
        plt.savefig(os.path.join(example_save_folder, 'affine_params10.png'))
        plt.close()
        for k in losses_generator.keys():
            print(k, losses_generator[k])
        break
        '''

    cols = ['Dice', 'HD', 'MSD']
    es2ed_df = pd.DataFrame(np.array(es2ed_all), columns=cols)
    es2ed_df.to_csv(os.path.join(example_save_folder, '4ch_es2ed_metrics.csv'))

