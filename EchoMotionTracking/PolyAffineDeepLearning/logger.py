'''
Codes adapted from https://github.com/AliaksandrSiarohin/first-order-model
'''

import numpy as np
import torch
import torch.nn.functional as F
import imageio
import os
from skimage.draw import circle
import matplotlib.pyplot as plt
import collections

class Logger:
    def __init__(self, log_dir, checkpoint_freq=100, visualizer_params=None, zfill_num=8, log_file_name='log.txt'):

        self.loss_list = []
        self.cpk_dir = log_dir
        self.visualizations_dir = os.path.join(log_dir, 'train-vis')
        if not os.path.exists(self.visualizations_dir):
            os.makedirs(self.visualizations_dir)
        self.log_file_train = open(os.path.join(log_dir, 'log_train.txt'), 'a')
        self.log_file_valid = open(os.path.join(log_dir, 'log_valid.txt'), 'a')
        self.zfill_num = zfill_num
        self.visualizer = Visualizer(**visualizer_params)
        self.checkpoint_freq = checkpoint_freq
        self.epoch = 0

        self.best_loss = float('inf')
        self.names = None
        self.best_valid_score = np.inf
        self.save_model = False
        self.counter = 0
        self.patience = 10
        self.early_stop = False

    def log_scores(self, loss_names, train):
        loss_mean = np.array(self.loss_list).mean(axis=0)
        loss_sum = np.sum(loss_mean)
        if (not train) and loss_sum < self.best_valid_score:
            self.best_valid_score = loss_sum
            self.save_model = True
            self.counter = 0
            self.early_stop = False
        if (not train) and loss_sum > self.best_valid_score:
            self.save_model = False
            self.counter += 1
            if self.counter >= self.patience:
                self.early_stop = True
                self.save_model = True

        loss_string = "; ".join(["%s - %.5f" % (name, value) for name, value in zip(loss_names, loss_mean)])
        loss_string = str(self.epoch).zfill(self.zfill_num) + ") " + loss_string

        if train:
            print(loss_string, file=self.log_file_train)
            self.loss_list = []
            self.log_file_train.flush()
        else:
            print(loss_string, file=self.log_file_valid)
            self.loss_list = []
            self.log_file_valid.flush()

        if self.save_model:
            self.save_cpk()
        out_dict = {}
        for name, value in zip(loss_names, loss_mean):
            out_dict[name] = value
        return out_dict

    def visualize_rec(self, inp, out):
        image = self.visualizer.visualize(inp, out)
        imageio.imsave(os.path.join(self.visualizations_dir, "%s-rec.png" % str(self.epoch).zfill(self.zfill_num)),
                       image)

    def save_cpk(self, emergent=False):
        cpk = {k: v.state_dict() for k, v in self.models.items()}
        cpk['epoch'] = self.epoch
        cpk_path = os.path.join(self.cpk_dir, '%s-checkpoint.pth.tar' % str(self.epoch).zfill(self.zfill_num))
        if not (os.path.exists(cpk_path) and emergent):
            torch.save(cpk, cpk_path)

    @staticmethod
    def load_cpk(checkpoint_path, generator=None, discriminator=None, kp_detector=None,
                 optimizer_generator=None, optimizer_discriminator=None, optimizer_kp_detector=None):
        if torch.cuda.is_available():
            map_location = None
        else:
            map_location = 'cpu'
        checkpoint = torch.load(checkpoint_path, map_location)
        if generator is not None:
            generator.load_state_dict(checkpoint['generator'])
        if kp_detector is not None:
            kp_detector.load_state_dict(checkpoint['kp_detector'])
        if discriminator is not None:
            try:
                discriminator.load_state_dict(checkpoint['discriminator'])
            except:
                print('No discriminator in the state-dict. Dicriminator will be randomly initialized')
        if optimizer_generator is not None:
            optimizer_generator.load_state_dict(checkpoint['optimizer_generator'])
        if optimizer_discriminator is not None:
            try:
                optimizer_discriminator.load_state_dict(checkpoint['optimizer_discriminator'])
            except RuntimeError as e:
                print('No discriminator optimizer in the state-dict. Optimizer will be not initialized')
        if optimizer_kp_detector is not None:
            optimizer_kp_detector.load_state_dict(checkpoint['optimizer_kp_detector'])

        return checkpoint['epoch']

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if 'models' in self.__dict__:
            self.save_cpk()
        self.log_file_train.close()
        self.log_file_valid.close()

    def log_iter(self, losses):
        losses = collections.OrderedDict(losses.items())
        if self.names is None:
            self.names = list(losses.keys())
        self.loss_list.append(list(losses.values()))

    def log_epoch(self, epoch, models=None, inp=None, out=None, train=True):
        self.epoch = epoch
        self.models = models
        # if (self.epoch + 1) % self.checkpoint_freq == 0:
        # self.save_cpk()
        out_dict = self.log_scores(self.names, train)
        self.visualize_rec(inp, out)
        return out_dict


class Visualizer:
    def __init__(self, kp_size=5, draw_border=False, colormap='gist_rainbow'):
        self.kp_size = kp_size
        self.draw_border = draw_border
        self.colormap = plt.get_cmap(colormap)

    def draw_image_with_kp(self, image, kp_array):
        image = np.tile(np.copy(image), (1, 1, 3))
        spatial_size = np.array(image.shape[:2][::-1])[np.newaxis]
        kp_array = spatial_size * (kp_array + 1) / 2
        num_kp = kp_array.shape[0]
        for kp_ind, kp in enumerate(kp_array):
            rr, cc = circle(kp[1], kp[0], self.kp_size, shape=image.shape[:2])
            image[rr, cc] = np.array(self.colormap(kp_ind / num_kp))[:3]
        return image

    def create_image_column_with_kp(self, images, kp):
        image_array = np.array([self.draw_image_with_kp(v, k) for v, k in zip(images, kp)])
        return self.create_image_column(image_array)

    def create_image_column(self, images):
        if self.draw_border:
            images = np.copy(images)
            images[:, :, [0, -1]] = (1, 1, 1)
        return np.concatenate(list(images), axis=0)

    def create_image_grid(self, *args):
        out = []
        for arg in args:
            if type(arg) == tuple:
                out.append(self.create_image_column_with_kp(arg[0], arg[1]))
            else:
                out.append(self.create_image_column(arg))
        return np.concatenate(out, axis=1)

    def visualize(self, inp, out):
        images = []
        # images with PAM original
        num_f = len(inp['video'])

        # Source image with keypoints
        source = inp['video'][inp['ED']:inp['ED'] + 1].repeat(num_f, 1, 1, 1).data.cpu().numpy()
        kp_source = out['value'][inp['ED']:inp['ED'] + 1].repeat(num_f, 1, 1).data.cpu().numpy()
        source = np.transpose(source, [0, 2, 3, 1])
        # print(source.shape, kp_source.shape)
        images.append((source, kp_source))

        # Transformed source image with keypoints
        trans_source = out['deformed_rest2ed'].data.cpu().numpy()
        kp_trans_source = out['prediction_kp_rest2ed'].data.cpu().numpy()
        trans_source = np.transpose(trans_source, [0, 2, 3, 1])
        # print(trans_source.shape, kp_trans_source.shape)
        images.append((trans_source, kp_trans_source))

        # Target image with keypoints
        target = inp['video'].data.cpu().numpy()
        kp_target = out['value'].data.cpu().numpy()
        target = np.transpose(target, [0, 2, 3, 1])
        # print(target.shape, kp_target.shape)
        images.append((target, kp_target))

        # Transformed all to reference
        trans_target = out['deformed_ed2rest'].data.cpu().numpy()
        kp_trans_target = out['prediction_kp_ed2rest'].data.cpu().numpy()
        trans_target = np.transpose(trans_target, [0, 2, 3, 1])
        # print(trans_target.shape, kp_trans_target.shape)
        images.append((trans_target, kp_trans_target))

        image = self.create_image_grid(*images)
        image = (255 * image).astype(np.uint8)
        return image
