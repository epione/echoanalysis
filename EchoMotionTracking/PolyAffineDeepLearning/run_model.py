import matplotlib.pyplot as plt
import numpy as np
import torch
import yaml
import os
from skimage.transform import resize
from modules.seq_motion import SequenceMotion, LossMotion
from scipy.io import loadmat, savemat
import imageio
import shutil
from matplotlib import animation


def to_var(x, device):
    if isinstance(x, np.ndarray):
        x = torch.from_numpy(x)
    x = x.to(device, dtype=torch.float)
    return x


def to_numpy(x):
    if not (isinstance(x, np.ndarray) or x is None):
        if x.is_cuda:
            x = x.data.cpu()
        x = x.detach().numpy()
    return x


def save_sq_pts_gif(img_sq, pt_sq, mask, save_folder, name):
    '''
    Save a gif with key points on original image sequences
    '''
    num_slice = len(pt_sq)
    tmp = save_folder + '/' + name
    os.makedirs(tmp, exist_ok=True)
    for f in range(num_slice):
        fig = plt.figure(figsize=(6, 6))
        ax1 = fig.add_subplot(111)
        ax1.imshow(img_sq[f], cmap='gray')
        if mask is not None:
            ax1.imshow(mask[f], alpha=0.4)
        ax1.scatter(pt_sq[f][:, 0], pt_sq[f][:, 1], s=80, label='Motion Estimation')
        ax1.axis('off')
        plt.legend()
        plt.savefig(os.path.join(tmp, '{}.png'.format(f)),
                    transparent=False,
                    facecolor='white'
                    )
        plt.close()
    frames = []
    for t in range(num_slice):
        image = imageio.imread(os.path.join(tmp, '{}.png'.format(t)))
        frames.append(image)
    imageio.mimsave(os.path.join(save_folder, name + '_img_sq.gif'),  # output gif
                    frames,  # array of input frames
                    fps=20)
    shutil.rmtree(tmp)


# save sequence gif
def save_tracking_gif(img_sq, pts=None, kpts=None, myo=None, save_folder='', name='', s=1.5):
    fig = plt.figure(figsize=(8, 8))
    plt.tight_layout()
    ax1 = fig.add_subplot(111)

    if pts is not None:
        idy, idx = np.where(myo)

    def init():
        ax1.imshow(img_sq[0], cmap='gray')
        if pts is not None:
            ax1.scatter(pts[0, 0, idy, idx], pts[0, 1, idy, idx], s=s)
        if kpts is not None:
            ax1.scatter(kpts[0,:, 0], kpts[0,:,1], s=85, color='tab:orange')
        ax1.axis('off')

    def animate(i):
        ax1.clear()
        ax1.imshow(img_sq[i], cmap='gray')
        if pts is not None:
            ax1.scatter(pts[i, 0, idy, idx], pts[i, 1, idy, idx], s=s)
        if kpts is not None:
            ax1.scatter(kpts[i,:, 0], kpts[i,:,1], s=85, color='tab:orange')
    num_slice = len(img_sq)
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=num_slice, interval=100, blit=False)
    Writer = animation.writers['pillow']
    writer = Writer(fps=20, bitrate=1800)
    anim.save(os.path.join(save_folder, name + '_tracking_sq.gif'), writer=writer)
    plt.close()


def Myo_region(img_sq, mask_ed, mask_es):
    '''
    Obtain the myocardium covered region using ED+ES masks
    :param img_sq: original image sequence, the one that was resized to [256,256] for segmentation
    :param mask_ed: [256,256] from segmentation model
    :param mask_es: [256,256] from segmentation model
    :return: cropped image sequence, cropped masks
    '''
    # check myo position
    _, hi, wi = img_sq.shape
    h, w = mask_ed.shape
    yy, xx = np.where(mask_ed == 2) # myocardium label = 2
    x0, x00 = int(max(np.min(xx) - 20, 0)), int(min(np.max(xx) + 20, w))
    y0, y00 = int(max(np.min(yy) - 20, 0)), int(min(np.max(yy) + 20, h))
    w0, h0 = x00 - x0, y00 - y0
    if w0 < h0:
        semi = int(h0 / 2)
    else:
        semi = int(w0 / 2)
    xs, xe = int(max((x00 + x0) / 2. - semi, 0)), int(min((x00 + x0) / 2. + semi, w))
    ys, ye = int(max((y00 + y0) / 2. - semi, 0)), int(min((y00 + y0) / 2. + semi, h))
    mask_ed0 = mask_ed[ys:ye, xs:xe]
    mask_es0 = mask_es[ys:ye, xs:xe]
    xs, xe = int(xs / 256 * wi), int(xe / 256 * wi)
    ys, ye = int(ys / 256 * hi), int(ye / 256 * hi)
    img_sq0 = img_sq[:, ys:ye, xs:xe]
    return img_sq0, mask_ed0, mask_es0, np.array([ys, ye, xs, xe])


def model_evaluation(generator, loss_generator, device, x, save_folder=None, name=None):
    x['video'] = torch.tensor(np.expand_dims(x['video'], 1)).to(device, dtype=torch.float)
    x['ED_MYO'] = torch.tensor(x['ED_MYO']).to(device, dtype=torch.float).unsqueeze(0)
    x['ES_MYO'] = torch.tensor(x['ES_MYO']).to(device, dtype=torch.float).unsqueeze(0)

    kp_pts, generated = generator(x)
    losses_generator, generated = loss_generator(x, kp_pts, generated)
    dis_list = generated['deformation_rest2ed'].detach().cpu().numpy()

    if save_folder is not None:
        kpts = (kp_pts['value'].detach().cpu().numpy() + 1) * 64
        save_sq_pts_gif(to_numpy(x['video'])[:,0], kpts, None, save_folder, name)

    return dis_list, kp_pts['value'].detach().cpu().numpy(), \
           kp_pts['jacobian'].detach().cpu().numpy()


def read_img_mask(img_path, mask_path, imgsize=128, myo=True):
    '''
    Customised function to read image + segmentation mask inputs
    '''
    ed, es = 0, -1 # need to set accordingly, does not influence key point and consecutive motion estimation
    # but they are important to study tracking from ED to ES
    img_sq = np.load(img_path)
    # ATTENTION: mask_sq is supposed to have [256,256] size per frame
    mask_sq = np.load(mask_path)
    mask_ed = mask_sq[ed]
    mask_es = mask_sq[es]
    _, ho, wo = img_sq.shape
    xresol, yresol = 1, 1

    bbox = np.array([0, 0, 0, 0])
    if myo:
        img_sq, mask_ed, mask_es, bbox = Myo_region(img_sq, mask_ed, mask_es)
        bbox[:2] = bbox[:2] / ho * imgsize
        bbox[2:] = bbox[2:] / wo * imgsize
    _, h, w = img_sq.shape
    resol = [h * yresol / imgsize, w * xresol / imgsize]
    img_sq = resize(img_sq.astype(float), (len(img_sq), imgsize, imgsize), 1)
    img_sq = img_sq / np.max(img_sq)
    mask_ed = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_ed.astype(np.int16)], -1, 0)
    mask_es = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_es.astype(np.int16)], -1, 0)
    mask_ed = np.round(resize(mask_ed.astype(float), (4, imgsize, imgsize), 0)).astype(np.int16)
    mask_es = np.round(resize(mask_es.astype(float), (4, imgsize, imgsize), 0)).astype(np.int16)
    x = {}

    x['video'] = img_sq
    x['ED_MYO'] = mask_ed[2:3]
    x['ES_MYO'] = mask_es[2:3]
    x['ED'] = ed
    x['ES'] = es
    x['bbox'] = bbox
    return x, resol, mask_ed, mask_es


# ==========================================================================================================
# Set model paths (TO CHANGE)
# ==========================================================================================================
model_path = ''
config_file = os.path.join(model_path, 'echonet-128.yaml')
dict_data = np.load('') # kpts prior information (not used during inference, only for loss calculation)
# ==========================================================================================================
with open(config_file) as f:
    config = yaml.load(f)
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

img_size = 128
generator = SequenceMotion(config['train_params'],
                       config['model_params']['common_params'],
                       config['model_params']['kp_detector_params'],
                       config['model_params']['dense_motion_params'],device = device)
generator.to(device)
loss_generator = LossMotion(config['train_params'], config['model_params']['common_params'],
                            dict_data, device=device, img_loss='NCC')
loss_generator.to(device)

checkpoint_path = model_path + '/00000037-checkpoint.pth.tar' # trained model
checkpoint = torch.load(checkpoint_path, device)
generator.load_state_dict(checkpoint['generator'])


# ==========================================================================================================
# Read image + mask (mask is used to crop images so that the LV is in the center for better tracking)
# ==========================================================================================================
img_path = ''
mask_path = ''
name = 'test'
example_save_folder = ''
x, resol, mask_ed, mask_es = read_img_mask(img_path, mask_path, imgsize=128, myo=True)
# estimate tracking information
dis_mat, kp_pts, jacobians = model_evaluation(generator, loss_generator, device, x,
                                              example_save_folder, name.split('.')[0])

# Save kep point postion + jacobian from each frame to the reference frame
savemat(os.path.join(example_save_folder, name.split('.')[0] + '_kpt+jacobian+track.mat'),
        {
            'img': x['video'][:, 0].detach().cpu().numpy(),
            'mask_ed': x['ED_MYO'][0, 0].detach().cpu().numpy(),
            'ed': x['ED'],
            'kpt': kp_pts,
            'jacobian': jacobians,
            'tracking': dis_mat
        })

img_sq = x['video'].detach().numpy()[:,0]
kp_pts = (kp_pts + 1)*63.5
grid = np.stack(np.meshgrid(np.arange(128), np.arange(128)), axis=0)
dis_mat = (dis_mat + 1) * 63.5
pos_sq = np.concatenate([np.expand_dims(grid, 0), np.transpose(dis_mat, (0,3,1,2))], axis=0)

save_sq_pts_gif(img_sq, kp_pts, None, example_save_folder, name)

save_tracking_gif(img_sq, pos_sq, kp_pts, mask_ed[2], example_save_folder, name, s=1.5)