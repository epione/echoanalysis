'''
Author: Yingyu Yang
'''

import numpy as np
import os
import pandas as pd
import skimage
import skimage.transform
import torch.utils.data

# ====================================================================================================================
# ECHONET
# ====================================================================================================================
def split_echonet(csv):
    df = pd.read_csv(csv)
    cur_df = df[df['NumberOfFrames'] > 50] # only taking examples with number of frames larger than 50
    bad_files = ['0X4EA078CC4E65B6A3.avi', '0X5DD5283AC43CCDD1.avi', '0X234005774F4CB5CD.avi',
                 '0X2DC68261CBCC04AE.avi', '0X35291BE9AB90FB89.avi', '0X6C435C1B417FDE8A.avi',
                 '0X5515B0BD077BE68A.avi'] # some files that cannot be processed properly, don't know why 
    cur_files = cur_df['FileName'].values
    new_files = list(set(cur_files).difference(set(bad_files)))
    tmp_df = pd.DataFrame(new_files, columns=['FileName'])
    cur_df = pd.merge(cur_df, tmp_df, on='FileName', how='inner')
    cur_df = cur_df[cur_df['ED'] < cur_df['ES']]
    train_files = cur_df[cur_df['Split'] == 'TRAIN']  # [:100]
    valid_files = cur_df[cur_df['Split'] == 'VAL']  # [:100]
    test_files = cur_df[cur_df['Split'] == 'TEST']
    return train_files, valid_files, test_files


class EchoEchonetDataset(torch.utils.data.Dataset):
    # Return img + mask (ED + ES)
    def __init__(self, files_csv, img_path, is_train=True, imgsize=128, transform=None, maxlength=50):
        super(EchoEchonetDataset, self).__init__()
        self.files = files_csv
        self.images_path = img_path
        self.imgsize = imgsize
        self.transform = transform
        self.is_train = is_train
        self.maxlength = maxlength

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        patient = self.files.iloc[idx]
        img_sq, ed, es, ed_myo, es_myo, ed_lv, es_lv, resol = self.load(patient)
        ff = self.maxlength
        if es > ed:
            start, stop = int(ed), int(ed) + ff
            frame_ed = 0
            dif = es - ed
            if dif >= ff:
                dif = 11
            frame_es = dif
            if min(len(img_sq), int(ed) + ff) != int(ed) + ff:
                stop = len(img_sq)
                start = max(len(img_sq) - ff, 0)
                frame_ed = int(ed) + min(ff - len(img_sq), 0)
                frame_es = min(frame_ed + dif, ff - 1)
        else:
            start, stop = int(es), int(es) + ff
            frame_es = 0
            dif = ed - es
            if dif >= ff:
                dif = 11
            frame_ed = dif
            if min(len(img_sq), int(es) + ff) != int(es) + ff:
                stop = len(img_sq)
                start = max(len(img_sq) - ff, 0)
                frame_es = int(es) + min(ff - len(img_sq), 0)
                frame_ed = min(frame_es + dif, ff - 1)
        img_out = img_sq[start:stop]
        img_out = (img_out - img_out.min()) / (img_out.max() - img_out.min())

        out = {}
        video = np.expand_dims(img_out, axis=1)
        out['ED'] = frame_ed
        out['ES'] = frame_es
        out['video'] = video
        out['name'] = patient['FileName']
        out['ED_MYO'] = np.expand_dims(ed_myo, axis=0)
        out['ES_MYO'] = np.expand_dims(es_myo, axis=0)
        out['ED_LV'] = np.expand_dims(ed_lv, axis=0)
        out['ES_LV'] = np.expand_dims(es_lv, axis=0)
        out['resol'] = resol
        return out

    def load(self, entry):
        filename = entry['FileName']
        ed, es = int(entry['ED']), int(entry['ES'])
        hori, wori = int(entry['FrameHeight']), int(entry['FrameWidth'])
        # read image
        patient_path_data = os.path.join(self.images_path, filename.split('.')[0] + '.npy')
        img_sq = np.load(patient_path_data)
        hlv, wlv = img_sq.shape[1], img_sq.shape[2]
        resol = [1, 1]  # [hlv/hori, wlv/wori] #y,x
        # resize image
        img_sq = skimage.transform.resize(img_sq, (img_sq.shape[0], self.imgsize, self.imgsize), order=1)
        # ed myo mask
        try:
            ed_mask = np.load(os.path.join(self.images_path, filename.split('.')[0] + '_ED.npy'))
            ed_lv, ed_myo = ed_mask[0], ed_mask[1]
            ed_myo = np.round(skimage.transform.resize(ed_myo.astype(float), (self.imgsize, self.imgsize), order=1))
            ed_lv = np.round(skimage.transform.resize(ed_lv.astype(float), (self.imgsize, self.imgsize), order=1))
            # es myo mask
            es_mask = np.load(os.path.join(self.images_path, filename.split('.')[0] + '_ES.npy'))
            es_lv, es_myo = es_mask[0], es_mask[1]
            es_myo = np.round(skimage.transform.resize(es_myo.astype(float), (self.imgsize, self.imgsize), order=1))
            es_lv = np.round(skimage.transform.resize(es_lv.astype(float), (self.imgsize, self.imgsize), order=1))
        except:
            ed_myo = np.ones((self.imgsize, self.imgsize))
            es_myo = np.ones((self.imgsize, self.imgsize))
            ed_lv = np.ones((self.imgsize, self.imgsize))
            es_lv = np.ones((self.imgsize, self.imgsize))
        return img_sq, ed, es, ed_myo, es_myo, ed_lv, es_lv, resol
