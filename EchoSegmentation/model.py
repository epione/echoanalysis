import torch
import torch.nn as nn
import torch.utils.data
import numpy as np


#=====================================================
# Utility functions
#=====================================================
def to_var(x, device):
    if isinstance(x, np.ndarray):
        x = torch.from_numpy(x)
    x = x.to(device, dtype=torch.float)
    return x


def to_numpy(x):
    if not (isinstance(x, np.ndarray) or x is None):
        if x.is_cuda:
            x = x.data.cpu()
        x = x.detach().numpy()
    return x


def get_activation(activation,inplace_flag =True):
    activation = activation.lower()
    if activation == 'ReLU':
        act = nn.ReLU(inplace=inplace_flag)
    elif activation =='Softmax':
        act = nn.Softmax(dim=1)
    elif activation =='Sigmoid':
        act = nn.Sigmoid()
    elif hasattr(nn, activation):
        act = getattr(nn, activation)(inplace=inplace_flag)
    else:
        act = nn.ReLU(inplace=inplace_flag)
    return act


# Convolution + Batch Normalization + Activation block
class ConvBatchAct(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size = 3, padding = 1, activation='ReLU'):
        super(ConvBatchAct, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, padding=padding)
        self.batchnorm = nn.BatchNorm2d(out_channels)
        self.act = get_activation(activation)

    def forward(self, x):
        out = self.conv(x)
        x_out = self.batchnorm(out)
        return self.act(x_out)


class UpBlock(nn.Module):
    # Upblock : upsample /transpose conv + ConvBatchAct block
    # Upblock has skip-connect block
    def __init__(self, in_channels, out_channels, activation='ReLU', upmode='upsample', skip_ch=0):
        super(UpBlock, self).__init__()
        self.skip_ch = skip_ch
        if upmode == 'upsample':
            self.up = nn.Upsample(scale_factor=2)
            self.conv = ConvBatchAct(in_channels + self.skip_ch, out_channels, activation=activation)
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels, kernel_size=3, padding=1, stride=2, output_padding=1)
            self.conv = ConvBatchAct(in_channels + self.skip_ch , out_channels, activation=activation)

    def forward(self, x, c=None):
        out = self.up(x)
        if self.skip_ch > 0:
            out = torch.cat([out, c], dim=1)
        return self.conv(out)


class DownBlock(nn.Module):
    def __init__(self, in_channels, out_channels, activation='ReLU'):
        super(DownBlock, self).__init__()
        self.maxpool = nn.MaxPool2d(2)
        self.conv = ConvBatchAct(in_channels, out_channels, activation=activation)

    def forward(self, x):
        out = self.maxpool(x)
        return self.conv(out)


class Encoder(nn.Module):
    def __init__(self, in_channels, out_channels, activation='ReLU'):
        super(Encoder, self).__init__()
        self.n_block = len(in_channels)
        assert (in_channels[1:] == out_channels[:-1])
        self.block = nn.Sequential()
        for i in range(self.n_block):
            self.block.add_module('DownBlock' + str(i + 1), DownBlock(in_channels[i], out_channels[i], activation))

    def forward(self, x):
        out = []
        for i in range(self.n_block):
            k = list(dict(self.block.named_children()).keys()).index('DownBlock' + str(i + 1))
            x = self.block[k](x)
            out.append(x)
        return out


class Decoder(nn.Module):
    def __init__(self, in_channels, out_channels, skip_chs, activation='ReLU'):
        super(Decoder, self).__init__()
        self.n_block = len(in_channels)
        self.block = nn.Sequential()
        for i in range(self.n_block):
            self.block.add_module('UpBlock' + str(i + 1),
                                  UpBlock(in_channels[i], out_channels[i], activation, upmode = 'upsample',skip_ch=skip_chs[i]))

    def forward(self, x, cats):
        outs = []
        for k in range(self.n_block):
            g = list(dict(self.block.named_children()).keys()).index('UpBlock' + str(k + 1))
            #print(x.size())
            #print(cats[k].size())
            x = self.block[g](x, cats[k])
            outs.append(x)
        # outs is only the layer output without the skip connection part
        return outs

#================================================================================
# Segmentation network 
#================================================================================
class SegNetOnly(nn.Module):
    def __init__(self,mode='bilinear', num_class=4):
        super(SegNetOnly, self).__init__()
        self.mode = mode
        #======================================
        #Segmentation
        #======================================
        # First conv
        self.conv = ConvBatchAct(1, 32)
        # Encoder + decoder conv
        ec_ins = [32, 32, 64, 128, 128]
        ec_outs = [32, 64, 128, 128, 128]
        self.encoder = Encoder(ec_ins, ec_outs)
        dc_ins = [128, 128, 128, 64, 32]
        dc_outs = [128, 128, 64, 32, 16]
        skip_chs = [128, 128, 64, 32, 32]
        self.decoder1 = Decoder(dc_ins, dc_outs, skip_chs)
        self.final1_1 = ConvBatchAct(16, 8)
        self.final1_2 = ConvBatchAct(8, num_class, activation='Softmax')

    def forward(self, x):
        x1 = self.conv(x)
        ec_out = self.encoder(x1)
        features = []
        for i in range(len(ec_out)-2,-1,-1):
            features.append(ec_out[i])
        features.append(x1)

        dc_out1 = self.decoder1(ec_out[-1], features)
        out1_1 = self.final1_1(dc_out1[-1])
        out1_2 = self.final1_2(out1_1)
        return out1_2