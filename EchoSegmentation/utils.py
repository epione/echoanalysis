import torch
import numpy as np
import torch.nn.functional as F
from scipy.ndimage import morphology
import os
from skimage.measure import label
from skimage.morphology import binary_opening, binary_closing, square, dilation
from skimage import transform, measure
import matplotlib.pyplot as plt
from skimage.morphology import convex_hull_image
from skimage.measure import perimeter
import scipy

#=======================================================================================================================
# Utility functions
#=======================================================================================================================
def to_var(x, device):
    if isinstance(x, np.ndarray):
        x = torch.from_numpy(x)
    x = x.to(device, dtype=torch.float)
    return x


def to_numpy(x):
    if not (isinstance(x, np.ndarray) or x is None):
        if x.is_cuda:
            x = x.data.cpu()
        x = x.detach().numpy()
    return x


def save_checkpoint(state, save_path):
    if not os.path.isdir(save_path):
        os.makedirs(save_path)

    epoch = state['epoch']
    filename = save_path + '/' + \
               'model.{:02d}.pth.tar'.format(epoch)
    torch.save(state, filename)


def update_pred_mask(mask):
    new_mask = np.zeros(mask.shape)
    for i in range(4):
        new_layer = mask == i
        try:
            new_layer = getLargestCC(new_layer)
        except:
            pass
        new_mask += new_layer * i
    return new_mask


#=======================================================================================================================
# Loss functions
#=======================================================================================================================
def dice_loss(x, target):
    smooth = 0.01
    intersection = torch.sum(x * target, dim=(2, 3), keepdim=True)
    input_pow = torch.sum(x.pow(2), dim=(2, 3), keepdim=True)
    target_pow = torch.sum(target.pow(2), dim=(2, 3), keepdim=True)
    out = 1 - ((2. * intersection + smooth) /
               (input_pow + target_pow + smooth))
    return out.mean()


def contour_loss(x, target, device):
    if x.dim()<4:
        x = torch.unsqueeze(x, 1)
        target = torch.unsqueeze(target, 1)
    target = target[:, :, 1:-1, 1:-1]
    strain = -1.5
    #smooth = 1e-4

    a = torch.Tensor([[1, 0, -1],
                      [2, 0, -2],
                      [1, 0, -1]])
    a = a.view((1, 1, 3, 3)).to(device,dtype=torch.float)
    G_x = F.conv2d(x, a, padding=0)

    b = torch.Tensor([[1, 2, 1],
                      [0, 0, 0],
                      [-1, -2, -1]])
    b = b.view((1, 1, 3, 3)).to(device,dtype=torch.float)
    G_y = F.conv2d(x, b, padding=0)

    G = torch.abs(G_x) + torch.abs(G_y)

    target_f = torch.flatten(target + strain, start_dim=1)
    contour_f = torch.flatten(G, start_dim=1)# + smooth

    contourDistanceSum = torch.sum(target_f * contour_f, dim=1)
    return contourDistanceSum.mean()


def binary_mask(mask, gamma=20, T=0.5):
    return 1 / (1 + torch.exp(-gamma * (mask - T)))


#=======================================================================================================================
# Post processing
#=======================================================================================================================
# Functions for region clean
# https://stackoverflow.com/questions/47540926/get-the-largest-connected-component-of-segmentation-image
def getLargestCC(segmentation):
    # first filling hole and get rid of small points
    segmentation = binary_closing(segmentation, square(5))
    # then find the largest component
    labels = label(segmentation)
    assert(labels.max() != 0) # assume at least 1 CC
    largestCC = labels == np.argmax(np.bincount(labels.flat)[1:])+1
    return largestCC


def calculate_regions(mask):
    '''
    0 -> background
    1 -> left ventricle
    2 -> myocardium
    3 -> left atrium
    '''
    lv = mask == 1
    my = mask == 2
    la = mask == 3
    try:
        lv = getLargestCC(lv)
        my = getLargestCC(my)
        la = getLargestCC(la)
    except:
        pass
    lvmy = my ^ lv
    try:
        # filling holes
        lvmy = getLargestCC(lvmy)
    except:
        pass
    return lv, my, la, lvmy


#=======================================================================================================================
# Evaluation metrics
#=======================================================================================================================
def convexity(mask):
    return np.sum(mask)/np.sum(convex_hull_image(mask))


def simplicity(mask):
    return np.sqrt(4*np.pi*np.sum(mask))/(perimeter(mask))


def surfd(input1, input2, sampling=1, connectivity=1):
    input_1 = np.atleast_1d(input1.astype(np.bool))
    input_2 = np.atleast_1d(input2.astype(np.bool))

    conn = morphology.generate_binary_structure(input_1.ndim, connectivity)

    S = input_1 ^ morphology.binary_erosion(input_1, conn)
    Sprime = input_2 ^ morphology.binary_erosion(input_2, conn)

    # ~s is the negative of s
    dta = morphology.distance_transform_edt(~S, sampling)
    dtb = morphology.distance_transform_edt(~Sprime, sampling)

    sds = np.concatenate([np.ravel(dta[Sprime != 0]), np.ravel(dtb[S != 0])])
    return sds


def metrics(mask_, gt_, sampling=1):
    '''
    Taking to binary array of same shape as input
    This function compute the confusion matrix and use it to calculate
    Dice metrics, Sensitivity and Specificity
    Input : mask_, gt_ numpy array of identic shape (only 1 and 0)
    Output : List of 3 scores
    '''
    try:
        mask_ = getLargestCC(mask_)
    except:
        pass
    lnot = np.logical_not
    land = np.logical_and

    true_positive = np.sum(land((mask_), (gt_)))
    false_positive = np.sum(land((mask_), lnot(gt_)))
    false_negative = np.sum(land(lnot(mask_), (gt_)))
    true_negative = np.sum(land(lnot(mask_), lnot(gt_)))

    M = np.array([[true_negative, false_negative],
                  [false_positive, true_positive]]).astype(np.float64)
    metrics = {}
    metrics['Sensitivity'] = M[1, 1] / (M[0, 1] + M[1, 1] + 1e-5)
    metrics['Specificity'] = M[0, 0] / (M[0, 0] + M[1, 0] + 1e-5)
    metrics['Dice'] = 2 * M[1, 1] / (M[1, 1] * 2 + M[1, 0] + M[0, 1] + 1e-5)
    # metrics may be NaN if denominator is zero! use np.nanmean() while
    # computing average to ignore NaNs.

    surf_dis = surfd(mask_, gt_, sampling)
    # add hausdorff distance metric
    metrics['Hausdorff'] = surf_dis.max()

    # add mean distance metric
    metrics['Mean Distance Error'] = surf_dis.mean()
    metrics['Convexity_pred'] = convexity(mask_)
    metrics['Convexity_gt'] = convexity(gt_)
    metrics['Simplicity_pred'] = simplicity(mask_)
    metrics['Simplicity_gt'] = simplicity(gt_)
    return [metrics['Dice'], metrics['Sensitivity'], metrics['Specificity'], metrics['Hausdorff'],
            metrics['Mean Distance Error'], metrics['Convexity_pred'], metrics['Convexity_gt'],
            metrics['Simplicity_pred'], metrics['Simplicity_gt']]


def evalAllmetric(mask_, gt_, sampling=1):
    '''
    This functions takes as input two numpy array with labels between
    0, 1, 2 and 3
    '''
    ref_lv, ref_my, ref_la, ref_lvmy = calculate_regions(mask_)
    lv, my, la, lvmy = calculate_regions(gt_)

    lv_metrics = metrics(ref_lv, lv, sampling)
    my_metrics = metrics(ref_my, my, sampling)
    la_metrics = metrics(ref_la, la, sampling)
    lvmy_metrics = metrics(ref_lvmy, lvmy, sampling)

    return lv_metrics + my_metrics + la_metrics + lvmy_metrics


#=======================================================================================================================
# Calculation
#=======================================================================================================================
# Functions for EF Calculation
def generate_myo_mask(myo):
    neg_myo = 1 - myo
    return torch.cat([neg_myo, myo], dim = 1)

'''
The idea to get basal and apex landmarks: 
1. find the outer contours of LA and LV to get the intersection points between them; 
2. obtain the two end points of the contour intersection --> basal end points 
3. the farest point on the LV contour to mid-basal point --> apex point 

get_apex_basal: 
try: when the segmentation split over LA and LV is clear 
except: when the basal part is not connected, especailly when the LV prediction is not full inside MYO 
'''

def find_endpoints(img):
    # Kernel to sum the neighbours
    kernel = [[1, 1, 1],
              [1, 0, 1],
              [1, 1, 1]]
    # 2D convolution (cast image to int32 to avoid overflow)
    img_conv = scipy.signal.convolve2d(img.astype(np.int32), kernel, mode='same')
    # Pick points where pixel is 255 and neighbours sum 255
    endpoints = np.stack(np.where((img == 255) & (img_conv == 255)), axis=1)
    return endpoints


def modify_post_myo(myo):
    lv = binary_opening(convex_hull_image(myo)^myo, square(5))
    return np.array([myo,lv])


def get_apex_basal(mask):
    # when there is no intersection between LA and LV
    lv = mask[1]
    la = mask[3]
    try:
        lv = getLargestCC(lv)
        la = getLargestCC(la)
    except:
        pass
    try:
        lv = dilation(lv, square(3))
        la = dilation(la, square(3))
        coord = np.where(lv * la)
        rm_idx = np.argmax(coord[1])
        lm_idx = np.argmin(coord[1])
        basal = np.zeros((2, 2))
        # (y,x)
        basal[0] = np.array([coord[0][lm_idx], coord[1][lm_idx]])
        basal[1] = np.array([coord[0][rm_idx], coord[1][rm_idx]])
        mid_basal = np.mean(basal, axis=0)
    except:
        # reshape myo & lv
        myo, lv = modify_post_myo(mask[2])
        mask[2] = myo.copy()
        mask[1] = lv.copy()
        img = dilation(lv, square(3)) * dilation(myo, square(3)) * 255
        basal = find_endpoints(img)
        mid_basal = np.mean(basal, axis=0)
    myo_con = measure.find_contours(mask[1], 0.5)[0]
    dis = 0
    idx = -1
    count = 0
    for pt in myo_con:
        new_dis = np.linalg.norm(pt - mid_basal)
        if new_dis > dis:
            dis = new_dis
            idx = count
        count += 1
    apex = myo_con[idx]

    return basal, apex


def line_tan(p1,p2):
    y1,x1 = p1
    y2,x2 = p2
    a = (y2-y1)/(y1*x2-y2*x1)
    b = (x1-x2)/(y1*x2-y2*x1)
    return a,b


def get_view_slices(pt0, img, mask, num_slice=20, show=False):
    basal0, apex0 = pt0
    mid_basal0 = np.mean(basal0, axis=0)
    a_axe, b_axe = line_tan(mid_basal0, apex0)

    # update apex and mid_basal
    lv = getLargestCC(mask[1])
    apex = apex0.copy()
    mid_basal = mid_basal0.copy()
    step_y = np.abs(apex[0] - mid_basal[0]) / num_slice
    bn_list = []
    slice_img_all = np.zeros(img.shape)

    pt_end_list = []
    for i in range(1, num_slice + 1):
        slice_img = np.zeros(img.shape)
        slice_y = apex[0] + step_y * i
        slice_x = (-1 - b_axe * slice_y) / a_axe if a_axe != 0 else 0
        params = [b_axe, -a_axe, a_axe * slice_y - b_axe * slice_x]
        for k in range(256):
            y = int(round((-params[2] - params[0] * k) / params[1]))
            if y < 0 or y > 255:
                continue
            slice_img[y, k] = 1
            slice_img_all[y, k] = 1
        slice_img = slice_img * lv
        # need to sort the pts according to x-axis
        try:
            pts = np.array(np.where(slice_img > 0))
            pts = pts[:, pts[1].argsort()]
            pt_end = pts[:, [0, -1]]
            pt_end_list.append(pt_end)
        except:
            pass
    # calculate volume
    slice_width = []
    for i in range(len(pt_end_list)):
        pt_end = pt_end_list[i]
        pt_end = pt_end  # *resol
        d = np.linalg.norm(pt_end[:, 0] - pt_end[:, 1])
        slice_width.append(d)

    lv_length = np.linalg.norm(mid_basal - apex)
    h = lv_length / num_slice
    if show:
        plt.imshow(img, cmap='gray')
        plt.plot([apex[1], mid_basal[1]], [apex[0], mid_basal[0]], color='y')
        for i in range(len(pt_end_list)):
            pt_end = pt_end_list[i]
            plt.plot(pt_end[1, :], pt_end[0, :], color='b')
        mask_lv = mask[1].copy().astype(float)
        mask_lv[np.where(mask_lv == 0)] = np.nan
        plt.imshow(mask_lv, cmap='rainbow', alpha=0.4)
    return slice_width, h


def get_BiPlaneSimpson_volume(pt0_2ch, img_2ch, mask_2ch, pt0_4ch, img_4ch, mask_4ch, resol, num_slice=20, show=True):
    # Bi-plane Simpson
    # axe is apex---mid basal

    slice_width_2ch, h_2ch = get_view_slices(pt0_2ch, img_2ch, mask_2ch, num_slice=num_slice, show=show)
    slice_width_4ch, h_4ch = get_view_slices(pt0_4ch, img_4ch, mask_4ch, num_slice=num_slice, show=show)
    ab_plus = 0
    for a2, a4 in zip(slice_width_2ch, slice_width_4ch):
        ab_plus += a2 * a4
    volume = np.pi * ab_plus * (h_2ch + h_4ch) / 8
    resol_v = resol[0] * resol[1] * resol[1]
    volume *= resol_v
    volume /= 1000
    return volume


def get_SinPlaneSimpson_volume(pt0, img, mask, resol, num_slice=20, show=False):
    # Single-plane Simpson
    # axe is apex---mid basal

    slice_width, h = get_view_slices(pt0, img, mask, num_slice=num_slice, show=show)
    ab_plus = 0
    for a2 in slice_width:
        ab_plus += a2 * a2
    volume = np.pi * ab_plus * h / 4
    resol_v = resol[0] * resol[1] * resol[1]
    volume *= resol_v
    volume /= 1000
    return volume


def myo_get_simpson_volume(apex, basal, img, mask_myo, resol, num_slice=20, show=True):
    '''
    Used when the LV segmentation is not very nice while the MYO mask provides a quite good contour of LV.
    '''
    # one plane volume (not very accurate)
    # axe is apex---mid basal
    mid_basal = np.mean(basal, axis=0)
    a_axe, b_axe = line_tan(mid_basal, apex)

    step_y = np.abs(apex[0] - mid_basal[0]) / num_slice
    pt_end_list = []
    for i in range(1, num_slice + 1):
        slice_y = apex[0] + step_y * (i)
        slice_x = int((-1 - b_axe * slice_y) / a_axe) if a_axe != 0 else 0
        bn_slice = [b_axe, -a_axe, a_axe * slice_y - b_axe * slice_x]
        pt_end = []
        # first left part
        for k in range(slice_x, -1, -1):
            slice_y = int((-bn_slice[0] * k - bn_slice[2]) / bn_slice[1])
            if slice_y <= 255 and mask_myo[slice_y, k] > 0:
                pt_end.append([k, slice_y])
                break
        # then right part
        for k in range(slice_x, 256):
            slice_y = int((-bn_slice[0] * k - bn_slice[2]) / bn_slice[1])
            if slice_y <= 255 and mask_myo[slice_y, k] > 0:
                pt_end.append([k, slice_y])
                break
        # need to sort the pts according to x-axis
        if len(pt_end) == 2:
            pt_end = np.array(pt_end)
            pt_end_list.append(pt_end)

    # calculate volume
    sum_d2 = 0
    for i in range(len(pt_end_list)):
        pt_end = pt_end_list[i]
        pt_end = pt_end  # *resol
        d = np.linalg.norm(pt_end[0] - pt_end[1])
        sum_d2 += d ** 2

    lv_length = np.linalg.norm(mid_basal - apex)
    volume = np.pi / 4 * sum_d2 * lv_length / num_slice

    resol_v = resol[0] * resol[1] * resol[1]
    volume *= resol_v
    volume /= 1000

    if show:
        plt.imshow(img, cmap='gray')
        plt.plot([apex[1], mid_basal[1]], [apex[0], mid_basal[0]], color='y')
        # plt.plot(endo_con[:,1],endo_con[:,0])
        for i in range(len(pt_end_list)):
            pt_end = pt_end_list[i]
            plt.plot(pt_end[:, 0], pt_end[:, 1], color='b')
        plt.imshow(mask_myo, cmap='rainbow', alpha=0.4)
    return volume


#=======================================================================================================================
# Others
#=======================================================================================================================
class Update_Params():
    def __init__(self, best_params, patience=5, min_delta=0):
        self.patience = patience
        self.min_delta = min_delta
        self.counter = 0
        self.best_loss = None
        self.best_params = best_params
        self.update = False

    def __call__(self, loss, params):
        if self.best_loss is None:
            self.best_loss = loss
            self.update = False
        elif self.best_loss - loss > self.min_delta:
            self.best_params = params
            self.best_loss = loss
            self.counter = 0
            self.update = False
        elif self.best_loss - loss <= self.min_delta:
            self.counter += 1
            if self.counter >= self.patience:
                self.update = True


class Early_stopping():
    def __init__(self, patience=10, min_delta=0):
        self.patience = patience
        self.min_delta = min_delta
        self.counter = 0
        self.best_loss = None
        self.early_stop = False

    def __call__(self, val_loss):
        if self.best_loss is None:
            self.best_loss = val_loss
        elif self.best_loss - val_loss > self.min_delta:
            self.best_loss = val_loss
        elif self.best_loss - val_loss <= self.min_delta:
            self.counter += 1
            print(f"INFO: Early stopping counter {self.counter} of {self.patience}")
            if self.counter >= self.patience:
                print('INFO: Early stopping')
                self.early_stop = True


class LR_updater():
    def __init__(self, lr=1e-4, patience=5, min_delta=0):
        self.patience = patience
        self.min_delta = min_delta
        self.counter = 0
        self.best_loss = None
        self.lr_update = False
        self.cur_lr = lr

    def __call__(self, val_loss):
        if self.best_loss is None:
            self.best_loss = val_loss
        elif self.best_loss - val_loss > self.min_delta:
            self.best_loss = val_loss
            self.lr_update = False
            self.counter = 0
        elif self.best_loss - val_loss <= self.min_delta:
            self.counter += 1
            if self.counter >= self.patience:
                self.lr_update = True
                self.cur_lr = self.cur_lr * 0.5