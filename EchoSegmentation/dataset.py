import SimpleITK as sitk
import numpy as np
import os
import pandas as pd
import random
import skimage
import skimage.transform
import torch.utils.data
from scipy.ndimage import morphology
from skimage import exposure
from skimage import transform
from skimage.filters import unsharp_mask, gaussian
from sklearn.model_selection import KFold
import pickle


def get_train_test_files(test_fold, folder_prefix, ch='2ch', delete_poor = True):
    # CAMUS training data (450 patients)
    '''
    Split dataset into train, validation and test 3 parts, based on the fold number (test_fold), 10 folds in total
    :param test_fold: n, nth fold data will be regarded as test data, the other folds train + valid
    :param folder_prefix: folder path, where the overal information csv is saved
    :param ch: chamber view, 2 or 4 (2CH or 4CH)
    :param delete_poor: if ignore poor quality images
    :return: train file names, valid file names, test file names
    '''
    all_df = pd.read_csv(os.path.join(folder_prefix, 'train_all_info.csv'))
    valid_fold = (test_fold + 1) % 10
    if test_fold == 9:
        valid_fold = 10
    train_fold = set(np.arange(1, 11)).symmetric_difference(set([test_fold, valid_fold]))

    # test_files
    test_df = all_df[all_df['Fold'] == test_fold]
    if delete_poor:
        test_num = test_df[test_df['ImageQuality_2ch'] != 'Poor'][test_df['ImageQuality_4ch'] != 'Poor']['Number'].values
    else:
        test_num =  test_df['Number'].values
    test_files_2ch = np.array([test_num, np.array([2] * len(test_num))]).T
    test_files_4ch = np.array([test_num, np.array([4] * len(test_num))]).T

    # valid files
    valid_df = all_df[all_df['Fold'] == valid_fold]
    if delete_poor:
        valid_num = valid_df[valid_df['ImageQuality_2ch'] != 'Poor'][valid_df['ImageQuality_4ch'] != 'Poor'][
            'Number'].values
    else:
        valid_num = valid_df['Number'].values
    valid_files_2ch = np.array([valid_num, np.array([2] * len(valid_num))]).T
    valid_files_4ch = np.array([valid_num, np.array([4] * len(valid_num))]).T

    # train_files
    train_num = all_df[all_df.Fold.isin(train_fold)]['Number'].values
    train_files_2ch = np.array([train_num, np.array([2] * len(train_num))]).T
    train_files_4ch = np.array([train_num, np.array([4] * len(train_num))]).T
    if ch == '2ch':
        return train_files_2ch, valid_files_2ch, test_files_2ch
    elif ch == '4ch':
        return train_files_4ch, valid_files_4ch, test_files_4ch
    else:
        return np.concatenate([train_files_2ch, train_files_4ch]), np.concatenate(
            [valid_files_2ch, valid_files_4ch]), np.concatenate([test_files_2ch, test_files_4ch])


#==================================================================================
# Dataset for Segmentation
# CAMUS dataset
#==================================================================================

class EchoPairDataset(torch.utils.data.Dataset):
    # Return img + mask (ED + ES) + landmark maps + mask distance maps
    def __init__(self, files_list, img_path, imgsize=128, transform=None):
        super(EchoPairDataset, self).__init__()
        self.files = files_list
        self.images_path = img_path
        self.transform = transform  # flag
        self.imgsize = imgsize

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        patient = self.files[idx]
        # dist_ed/es is already scaled ~
        img_ed, mask_ed, dist_ed, img_es, mask_es, dist_es = self.load(patient)
        sampling = 1
        # we also need to load image seg regions (update regions)

        regions_path = self.images_path + 'patient' + str(int(patient[0])).zfill(4) + '/LM_gt/'
        with open(regions_path + str(int(patient[1])) + 'ch_ES_landmark_map' + str(self.imgsize) + '.pickle', 'rb') as f:
            out1 = pickle.load(f)
        apex_es = out1['apex']
        basal1_es = out1['basal1']
        basal2_es = out1['basal2']
        map_es = np.array([apex_es, basal1_es, basal2_es])

        with open(regions_path + str(int(patient[1])) + 'ch_ED_landmark_map' + str(self.imgsize) + '.pickle', 'rb') as f:
            out2 = pickle.load(f)
        apex_ed = out2['apex']
        basal1_ed = out2['basal1']
        basal2_ed = out2['basal2']
        map_ed = np.array([apex_ed, basal1_ed, basal2_ed])

        if self.transform:
            img_ed, mask_ed, dist_ed, sampling = self.transform((img_ed, mask_ed, dist_ed), size = [self.imgsize, self.imgsize])
            img_es, mask_es, dist_es, sampling = self.transform((img_es, mask_es, dist_es), size = [self.imgsize, self.imgsize])

        return patient[0], patient[1], img_ed, mask_ed, map_ed, dist_ed, img_es, mask_es, map_es, dist_es, sampling

    def load(self, ID):
        i, ch = ID
        i = int(i)
        ch = int(ch)
        id_pre = 'patient' + str(i).zfill(4) + '/patient' + str(i).zfill(4) + '_' + str(int(ch)) +'CH_'
        patient_path_ed = os.path.join(self.images_path, id_pre) + 'ED.mhd'
        img_ed = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_ed))
        patient_path_es = os.path.join(self.images_path, id_pre) + 'ES.mhd'
        img_es = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_es))

        mask_path_ed = os.path.join(self.images_path, id_pre) + 'ED_gt.mhd'
        mask_ed = sitk.GetArrayFromImage(sitk.ReadImage(mask_path_ed))
        mask_ed = mask_ed[0].astype(np.int16)
        mask_ed = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_ed], -1, 0)

        mask_path_es = os.path.join(self.images_path, id_pre) + 'ES_gt.mhd'
        mask_es = sitk.GetArrayFromImage(sitk.ReadImage(mask_path_es))
        mask_es = mask_es[0].astype(np.int16)
        mask_es = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_es], -1, 0)
        # read distance map
        map_path_ed = os.path.join(self.images_path, id_pre) + 'ED_gt_dis_map.mhd'
        dist_ed = sitk.GetArrayFromImage((sitk.ReadImage(map_path_ed)))
        map_path_es = os.path.join(self.images_path, id_pre) + 'ES_gt_dis_map.mhd'
        dist_es = sitk.GetArrayFromImage((sitk.ReadImage(map_path_es)))

        return img_ed, mask_ed, dist_ed, img_es, mask_es, dist_es


class Echo24CHDataset(torch.utils.data.Dataset):
    # Return image + masks (2ch ED+ES) (4ch ED+ES)
    def __init__(self, files_list, img_path, imgsize=128, transform=None):
        super(Echo24CHDataset, self).__init__()
        self.files = files_list
        self.images_path = img_path
        self.transform = transform  # flag
        self.imgsize = imgsize

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        patient = self.files[idx]
        img_ed_2ch, mask_ed_2ch, img_es_2ch, mask_es_2ch, img_ed_4ch, mask_ed_4ch, img_es_4ch, mask_es_4ch = self.load(patient)
        sampling = 1
        if self.transform:
            img_ed_2ch, mask_ed_2ch, _, sampling = self.transform((img_ed_2ch, mask_ed_2ch, None),
                                                                  size = [self.imgsize, self.imgsize])
            img_es_2ch, mask_es_2ch, _, sampling = self.transform((img_es_2ch, mask_es_2ch, None),
                                                                  size = [self.imgsize, self.imgsize])
            img_ed_4ch, mask_ed_4ch, _, sampling = self.transform((img_ed_4ch, mask_ed_4ch, None),
                                                                  size=[self.imgsize, self.imgsize])
            img_es_4ch, mask_es_4ch, _, sampling = self.transform((img_es_4ch, mask_es_4ch, None),
                                                                  size=[self.imgsize, self.imgsize])
        return patient[0], img_ed_2ch, mask_ed_2ch, img_es_2ch, mask_es_2ch, img_ed_4ch, mask_ed_4ch, img_es_4ch, mask_es_4ch, sampling

    def load(self, ID):
        i, ch = ID
        i = int(i)
        ch = 2
        id_pre = 'patient' + str(i).zfill(4) + '/patient' + str(i).zfill(4) + '_' + str(int(ch)) +'CH_'
        patient_path_ed = os.path.join(self.images_path, id_pre) + 'ED.mhd'
        img_ed_2ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_ed))
        patient_path_es = os.path.join(self.images_path, id_pre) + 'ES.mhd'
        img_es_2ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_es))

        mask_path_ed = os.path.join(self.images_path, id_pre) + 'ED_gt.mhd'
        mask_ed = sitk.GetArrayFromImage(sitk.ReadImage(mask_path_ed))
        mask_ed = mask_ed[0].astype(np.int16)
        mask_ed_2ch = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_ed], -1, 0)

        mask_path_es = os.path.join(self.images_path, id_pre) + 'ES_gt.mhd'
        mask_es = sitk.GetArrayFromImage(sitk.ReadImage(mask_path_es))
        mask_es = mask_es[0].astype(np.int16)
        mask_es_2ch = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_es], -1, 0)

        ch = 4
        id_pre = 'patient' + str(i).zfill(4) + '/patient' + str(i).zfill(4) + '_' + str(int(ch)) + 'CH_'
        patient_path_ed = os.path.join(self.images_path, id_pre) + 'ED.mhd'
        img_ed_4ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_ed))
        patient_path_es = os.path.join(self.images_path, id_pre) + 'ES.mhd'
        img_es_4ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_es))

        mask_path_ed = os.path.join(self.images_path, id_pre) + 'ED_gt.mhd'
        mask_ed = sitk.GetArrayFromImage(sitk.ReadImage(mask_path_ed))
        mask_ed = mask_ed[0].astype(np.int16)
        mask_ed_4ch = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_ed], -1, 0)

        mask_path_es = os.path.join(self.images_path, id_pre) + 'ES_gt.mhd'
        mask_es = sitk.GetArrayFromImage(sitk.ReadImage(mask_path_es))
        mask_es = mask_es[0].astype(np.int16)
        mask_es_4ch = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_es], -1, 0)

        return img_ed_2ch, mask_ed_2ch, img_es_2ch, mask_es_2ch, img_ed_4ch, mask_ed_4ch, img_es_4ch, mask_es_4ch


# ==================================================================================
# functions for data transformation (augmentation)
# Possible tranforms:
# 1. image quality: sharpening, blurring, noise
# 2. image appearance: brightness, contrast
# 3. spatial transforms: rotation, scaling and deformation
# ==================================================================================
def zoom(sample, size = [256, 256]):
    img, mask, map = sample
    # zoom
    if img is not None:
        k, y, x = img.shape
        resolution = [y / size[0] * 0.15, x / size[1] * 0.3]
        k = img.shape[0]
        img = transform.resize(img, (k, size[0], size[1]), 1)
    if mask is not None:
        k, y, x = mask.shape
        #resolution = [y / size[0] * 0.15, x / size[1] * 0.3]
        mask = np.round(transform.resize(mask.astype(float), (k, size[0], size[1]), 1))
    if map is not None:
        k, y, x = map.shape
        #resolution = [y / size[0] * 0.15, x / size[1] * 0.3]
        map = transform.resize(map, (k, size[0], size[1]), 1)

    return img, mask, map, resolution


def crop_resize(img, new_l, resol, limit=256):
    k, y, x = img.shape
    resolution = [y / new_l * resol[0], x / new_l * resol[1]]
    img = transform.resize(img, (k, new_l, new_l), 1)
    if new_l > limit:
        s = int((new_l - limit) / 2)
        img = img[:, s:(s + limit), s:(s + limit)]
    else:
        s1 = int((limit - new_l) / 2)
        s2 = limit - new_l - s1
        img = np.pad(img, ((0, 0), (s1, s2), (s1, s2)), mode='edge')
    return img, resolution


def rotation(img, angle):
    new_out = []
    for im in img:
        new_out.append(transform.rotate(im, angle, mode='edge'))
    return np.array(new_out)


def data_aug(sample, size=[256, 256], prob=0.5):
    img, mask, map = sample
    # zoom
    if img is not None:
        k, y, x = img.shape
        resolution = [y / size[0] * 0.15, x / size[1] * 0.3]
        # 0.15 and 0.3 specific for CAMUS (different resolution along x,y axis)
        img = transform.resize(img, (k, size[0], size[1]), 1)
    if mask is not None:
        k, y, x = mask.shape
        resolution = [y / size[0] * 0.15, x / size[1] * 0.3]
        mask = np.round(transform.resize(mask.astype(float), (k, size[0], size[1]), 1))
    if map is not None:
        k, y, x = map.shape
        #resolution = [y / size[0] * 0.15, x / size[1] * 0.3]
        map = transform.resize(map, (k, size[0], size[1]), 1)

    # rotation [-10,10]
    if random.random() > prob:
        angle = random.randint(-15, 15)
        if img is not None:
            img = rotation(img, angle)
        if mask is not None:
            mask = rotation(mask, angle)
        if map is not None:
            map = rotation(map, angle)

    # crop and scale [0.7-1.3]
    if random.random() > prob:
        scale = random.random() * 0.6 + 0.7
        new_l = int(scale * size[0])
        if img is not None:
            img, resolution = crop_resize(img, new_l, resolution, size[0])
        if mask is not None:
            mask, _ = crop_resize(mask, new_l, resolution, size[0])
        if map is not None:
            map, _ = crop_resize(map, new_l, resolution, size[0])

    # augmentation
    # brightness [0.9,1.1]
    if img is not None:
        new_img = []
        k, y, x = img.shape
        for f in range(k):
            img0 = img[f]
            if random.random() > prob:
                # image: I * gamma
                bright = random.random() * 0.2 + 0.9
                img0 = exposure.adjust_gamma(img0, bright)
                # no operation on mask

            # contrast [0.8,1.2]
            if random.random() > prob:

                # cutoff [0.4,0.6]
                cutoff = random.random() * 0.2 + 0.4
                # gain [4,10]
                gain = random.random() * 6 + 4
                img0 = exposure.adjust_sigmoid(img0, cutoff=cutoff, gain=gain)

            # sharpening: std [0.25-1.5] amount [1,2]
            if random.random() > prob:

                std = random.random() * 1.25 + 0.25
                amount = random.random() * 1 + 1
                img0 = unsharp_mask(img0, radius=std, amount=amount)

            # blurring std[0.25,1.5]
            if random.random() > prob:

                std = random.random() * 1.25 + 0.25
                img0 = gaussian(img0, sigma=std)

            # noise : speckle noise
            if random.random() > prob:
                # gaussian noise std [0.01,0.1]
                std = random.random() * 0.09 + 0.01
                gauss = np.random.normal(0, std, (size[0], size[1]))
                gauss = np.sqrt(img0) * gauss
                img0 = gauss + img0
            new_img.append(img0)
        img = np.array(new_img)

    return img, mask, map, resolution