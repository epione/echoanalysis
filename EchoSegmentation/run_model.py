'''
This script is to run trained segmentation model on various input data samples.
Such as EchoNet-Dynamic, HMC-QU and local private datasets etc.
'''

import torch
import os
from model import *
from utils import *
from time import time
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import seaborn as sns
from scipy.signal import find_peaks
from skimage import transform

def post_seg(seg):
    '''
    Post-process the predicted segmentation mask, keep the largest connected component
    :param seg:
    :return:
    '''
    out = np.zeros(seg.shape[1:])
    new_seg = []
    for i in range(len(seg)):
        re = getLargestCC(seg[i])
        out += re * i
    idx = np.where(out==4)
    out[idx] = 3
    return out


from matplotlib import animation
def save_sq_gif(img_sq, seg_sq, lm_sq, num, save_folder):
    '''
    Save animation of segmentation video to show the temporal change of masks, landmarks, aligned with original echo
    sequences
    :param img_sq: [f, h, w]
    :param seg_sq: [f, h, w]
    :param lm_sq: [f, 3, 2] [y,x]
    :param num: number to name the saved video
    :param save_folder: folder path to save the video
    :return:
    '''
    fig = plt.figure(figsize=(6,12))
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    # initialization function: plot the background of each frame
    def init():
        lm = lm_sq[0]
        seg = seg_sq[0].astype(float)
        idx = np.where(seg == 0)
        seg[idx] = np.nan
        ax1.imshow(img_sq[0], cmap='gray')
        ax1.axis('off')
        ax2.imshow(img_sq[0], cmap='gray')
        ax2.plot(lm[:,1], lm[:,0], linestyle='None', marker='o', markersize=10, label='Landmark')
        ax2.imshow(seg, alpha = 0.5)
        ax2.legend()
        ax2.axis('off')

    def animate(i):
        lm = lm_sq[i]
        seg = seg_sq[i].astype(float)
        idx = np.where(seg == 0)
        seg[idx] = np.nan
        ax1.clear()
        ax1.imshow(img_sq[i], cmap='gray')
        ax1.axis('off')
        ax2.clear()
        ax2.imshow(img_sq[i], cmap='gray')
        ax2.plot(lm[:,1], lm[:,0], linestyle='None', marker='o', markersize=10, label='Landmark')
        ax2.imshow(seg, alpha = 0.5)
        ax2.legend()
        ax2.axis('off')

    num_slice = len(img_sq)
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=num_slice, interval=100, blit=False)
    Writer = animation.writers['pillow']
    writer = Writer(fps=20, bitrate=1800)
    anim.save(os.path.join(save_folder, str(num) + '_img_seg_sq.gif'), writer=writer)
    plt.close()

# Please implement this function to read the image for model input and perform necessary preprocessing
def custom_image():
    img = np.load('')
    return img


# segmentation
#=====================================================================
# Set model
#============================================
# please modify as you need
array = custom_image()
save_folder = ''
sample_name = 'test'
model_prefix = ''
#============================================

fold = 1
model_path = model_prefix + '_fold' + str(fold) + '/'
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
seg_model = SegNetOnly()
seg_model.to(device)

predict_n_list = [69, 96, 86, 68, 41, 58, 60, 59, 52, 62]
predict_n = predict_n_list[fold-1]
model_name = 'model.' + str(predict_n).zfill(2) + '.pth.tar'
dictx = torch.load(os.path.join(model_path, model_name),map_location='cpu')
seg_model.load_state_dict(dictx['seg_model'])

#====================================================
# set image parameters
#====================================================
size = [256, 256] # should be [256,256]
resol_re = [1, 1]
num_f = len(array)
t1 = time()

#=====================================================================
# Predict segmentation (+landmarks) and calculate left ventricle volume
#=====================================================================
pred_seg = [] # to save segmentation
pred_vol = [] # to save volume of left ventricle blood pool
pred_lvl = [] # to save left ventricle length
pred_lms = [] # to save left heart landmarks (2 basal points + 1 apex point)
array_re_list = [] # to save resized images

with torch.no_grad():
    seg_model.eval()
    volume = 0
    lv_l = 0
    basal = np.array([[0, 0], [0, 0]])
    apex = np.array([0, 0])
    for i in range(num_f):
        arr_ori = array[i]
        # resize
        arr_re = transform.resize(arr_ori, (size[0], size[1]), 1)
        array_re_list.append(arr_re)
        # send to torch
        arr_torch = torch.unsqueeze(torch.unsqueeze(to_var(arr_re, device),0),0)
        # predict
        mask_pred = seg_model(arr_torch)
        mask_pred_np = np.argmax(to_numpy(mask_pred[0]), axis=0)
        mask_pred_np4 = np.rollaxis(np.eye(4, dtype=np.uint8)[mask_pred_np], -1, 0)
        pred_seg.append(mask_pred_np)

        #try:
        # Volume & EF
        basal, apex = get_apex_basal(mask_pred_np4)
        volume = get_SinPlaneSimpson_volume([basal, apex], arr_re, mask_pred_np4, resol_re, num_slice=20, show=False)
        pred_vol.append(volume)
        pred_lms.append(np.concatenate([basal, np.expand_dims(apex, axis=0)]))
        # Landmark LVLS
        lv_l = np.linalg.norm(np.mean(basal, axis=0) - apex)
        pred_lvl.append(lv_l)
        #except:
            #pred_vol.append(volume)
            #pred_lvl.append(lv_l)
            #pred_lms.append(np.concatenate([basal, np.expand_dims(apex, axis=0)]))

t2 =time()
print(t2-t1)
pred_seg= np.array(pred_seg)
pred_vol= np.array(pred_vol)
pred_lvl = np.array(pred_lvl)
pred_lms = np.array(pred_lms)
array_re_list = np.array(array_re_list)

# smooth temporal curve of volume and left ventricle length, please modify the parameters accordingly
smooth_vol = savgol_filter(pred_vol, 5, 3)
hidx, _ = find_peaks(smooth_vol,distance = len(smooth_vol)/5)
hidy = smooth_vol[hidx]
lidx, _ = find_peaks(-smooth_vol,distance = len(smooth_vol)/5)
lidy = smooth_vol[lidx]

smooth_lvl = savgol_filter(pred_lvl, 5, 3)
hidxl, _ = find_peaks(smooth_lvl, distance = len(smooth_lvl)/5)
hidyl = smooth_lvl[hidxl]
lidxl, _ = find_peaks(-smooth_lvl, distance = len(smooth_lvl)/5)
lidyl = smooth_lvl[lidxl]

# calculate ejection fraction and left ventricle length change ratio (or normalized MAPSE)
ef = (np.mean(smooth_vol[hidx]) - np.mean(smooth_vol[lidx]))/np.mean(smooth_vol[hidx])
mapsen = (np.mean(smooth_lvl[hidx]) - np.mean(smooth_lvl[lidx]))/np.mean(smooth_lvl[hidx])
print(ef, mapsen)

# save figures
sns.set_style('white')
plt.figure(figsize=(6,12))
plt.subplot(211)
plt.plot(np.arange(num_f), pred_vol, label = 'Raw')
plt.plot(np.arange(num_f), smooth_vol, label = 'Smooth')
plt.scatter(hidx, hidy, color = 'r', label = 'ED')
plt.scatter(lidx, lidy, color = 'b', label = 'ES')
plt.legend()
plt.title('Volume, EF({:.2f}%)'.format(ef*100))
plt.xlabel('Frame')
plt.ylabel('Volume/mL')
plt.subplot(212)
plt.plot(np.arange(num_f), pred_lvl, label = 'Raw')
plt.plot(np.arange(num_f), smooth_lvl, label = 'Smooth')
plt.scatter(hidxl, hidyl, color = 'r', label = 'ED')
plt.scatter(lidxl, lidyl, color = 'b', label = 'ES')
plt.legend()
plt.legend()
plt.title('LV length, LVS({:.2f}%)'.format(mapsen*100))
plt.xlabel('Frame')
plt.ylabel('Length/pxls')
plt.savefig(os.path.join(save_folder, 'vol_lvl_fig.png'))

# save prediction data
np.save(os.path.join(save_folder, 'vol_raw.npy'), pred_vol)
np.save(os.path.join(save_folder, 'lvl_raw.npy'), pred_lvl)
np.save(os.path.join(save_folder, sample_name + '_seq_segmentation.npy'), pred_seg)
save_sq_gif(array_re_list, pred_seg, pred_lms, sample_name, save_folder)
