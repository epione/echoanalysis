import torch
import os
from dataset import *
from model import *
from utils import *
from time import time
import numpy as np
import sys
import pandas as pd
'''
This script is used to train Segmentation model using the training data (450 patients) of CAMUS dataset. 
Based on the fold number, it splits the 450 patients into train, valid, test three parts. Valid data is 
used to choose the best performing model. Test data is used for performance evaluation. 
'''

#=======================================================================================================================
# Utility functions
#=======================================================================================================================


def print_loss(folder, loss_list, cur_cc, total_s, time, epoch, mode='Train'):
    sg_total_loss, dist_total_loss1, dist_total_loss2, total_loss = loss_list
    with open(folder + 'loss.txt', 'a') as f:
        f.write('{} Epoch {} ({}/{})'.format(mode, epoch, cur_cc, total_s) + "\n")
        f.write('Seg Loss Average {:.6f}'.format(sg_total_loss / cur_cc) + "\n")  # dice loss
        f.write('ContourENDO loss Average {:.6f}'.format(dist_total_loss1 / cur_cc) + "\n") # contour (ENDO) loss
        f.write('ContourEPI loss Average {:.6f}'.format(dist_total_loss2 / cur_cc) + "\n") # contour (EPI) loss
        f.write('Total Loss Average {:.6f}'.format(total_loss / cur_cc) + "\n")
        f.write('Time: {}({})'.format(time, time / cur_cc) + "\n")
    print('({}/{}: Seg {}, ConEndo {}, ConEpi {}, Total {})'.format(cur_cc, total_s,
                                                                    sg_total_loss / cur_cc,
                                                                    dist_total_loss1 / cur_cc,
                                                                    dist_total_loss2 / cur_cc,
                                                                    total_loss / cur_cc))


def train_seg_model(folder, loader, seg_net, device, epoch, lambs, optimizer, contour_flag = False):
    '''
    Train segmentation model for one epoch.
    :param folder: the path to save the trained model, where we also save the loss values
    :param loader: customized data loader object
    :param seg_net: segmentation model object
    :param device: device type
    :param epoch: epoch number
    :param lambs: lambdas for loss function sub-components
    :param optimizer: optimizer object
    :param contour_flag: if apply contour loss or not
    :return:
    '''
    sg_total_loss = torch.tensor(0, dtype=torch.float) # dice loss
    dist_total_loss1 = torch.tensor(0, dtype=torch.float) # contour ENDO loss
    dist_total_loss2 = torch.tensor(0, dtype=torch.float) # contour EPI loss
    total_loss = torch.tensor(0, dtype=torch.float) # total loss
    num_sample = 0 # count the number of samples
    t0 = time()
    print('====================={}ing Epoch {} | Mode {} ==================='.format('Train', epoch, 'SEG'))

    for i, sample in enumerate(loader, 1):
        num, ch, img1, mask1, map1, dist1, img2, mask2, map2, dist2, sampling = sample
        img1 = to_var(img1, device)
        mask1 = to_var(mask1, device)
        dist1 = to_var(dist1, device)
        img2 = to_var(img2, device)
        mask2 = to_var(mask2, device)
        dist2 = to_var(dist2, device)
        # for first image
        mask1_pred = seg_net(img1)
        sg_loss = dice_loss(mask1_pred, mask1)
        if contour_flag:
            # contour loss for ENDO (blood pool)
            dist_loss1 = contour_loss(binary_mask(mask1_pred[:, 1]), dist1[:, 1], device)
            # contour loss for EPI (blood pool + myocardium)
            dist_loss2 = contour_loss(binary_mask(mask1_pred[:, 1] + mask1_pred[:, 2]), dist1[:, 3], device)
        else:
            dist_loss1 = torch.tensor(0, dtype=torch.float)
            dist_loss2 = torch.tensor(0, dtype=torch.float)
        frame_loss = lambs[0] * sg_loss + lambs[1] * dist_loss1 + lambs[2] * dist_loss2
        optimizer.zero_grad()
        frame_loss.backward()  # (retain_graph=True)
        optimizer.step()
        sg_total_loss += sg_loss.data
        dist_total_loss1 += dist_loss1.data
        dist_total_loss2 += dist_loss2.data
        total_loss += frame_loss.data

        # for second image
        mask2_pred = seg_net(img2)
        sg_loss = dice_loss(mask2_pred, mask2)
        if contour_flag:
            dist_loss1 = contour_loss(binary_mask(mask2_pred[:, 1]), dist2[:, 1], device)
            dist_loss2 = contour_loss(binary_mask(mask2_pred[:, 1] + mask2_pred[:, 2]), dist2[:, 3], device)
        else:
            dist_loss1 = torch.tensor(0, dtype=torch.float)
            dist_loss2 = torch.tensor(0, dtype=torch.float)
        frame_loss = lambs[0] * sg_loss + lambs[1] * dist_loss1 + lambs[2] * dist_loss2
        optimizer.zero_grad()
        frame_loss.backward()  # (retain_graph=True)
        optimizer.step()
        sg_total_loss += sg_loss.data
        dist_total_loss1 += dist_loss1.data
        dist_total_loss2 += dist_loss2.data
        total_loss += frame_loss.data

        num_sample +=2
        t1 = time()
        if i % 50 == 0:
            print_loss(folder, [sg_total_loss, dist_total_loss1, dist_total_loss2, total_loss], num_sample, len(loader)*2, t1-t0, epoch, mode='Train')
    print_loss(folder, [sg_total_loss, dist_total_loss1, dist_total_loss2, total_loss], num_sample, len(loader)*2, t1-t0, epoch, mode='Train')
    return sg_total_loss/(len(loader)*2), dist_total_loss1/(len(loader)*2), dist_total_loss2/(len(loader)*2), total_loss/(len(loader)*2)


def valid_seg_model(folder, loader, seg_net, device, epoch, lambs):
    '''
    Validate the trained model using validation dataset, similar to train_seg_model
    :param folder: the path to save the trained model, where we also save the loss values
    :param loader: customized data loader object
    :param seg_net: segmentation model object
    :param device: device type
    :param epoch: epoch number
    :param lambs: lambdas for loss function sub-components
    :return: validation loss
    '''
    sg_total_loss = torch.tensor(0, dtype=torch.float)
    dist_total_loss1 = torch.tensor(0, dtype=torch.float)
    dist_total_loss2 = torch.tensor(0, dtype=torch.float)
    total_loss = torch.tensor(0, dtype=torch.float)
    num_sample = 0
    t0 = time()
    print('====================={}ing Epoch {} | Mode {} ==================='.format('Valid', epoch, 'SEG'))

    for i, sample in enumerate(loader, 1):
        num, ch, img1, mask1, map1, dist1, img2, mask2, map2, dist2, sampling = sample
        img1 = to_var(img1, device)
        mask1 = to_var(mask1, device)
        dist1 = to_var(dist1, device)
        img2 = to_var(img2, device)
        mask2 = to_var(mask2, device)
        dist2 = to_var(dist2, device)

        mask1_pred = seg_net(img1)
        sg_loss = dice_loss(mask1_pred, mask1)
        dist_loss1 = contour_loss(binary_mask(mask1_pred[:, 1]), dist1[:, 1], device)
        dist_loss2 = contour_loss(binary_mask(mask1_pred[:, 1] + mask1_pred[:, 2]), dist1[:, 3], device)
        frame_loss = lambs[0] * sg_loss + lambs[1] * dist_loss1 + lambs[2] * dist_loss2
        sg_total_loss += sg_loss.data
        dist_total_loss1 += dist_loss1.data
        dist_total_loss2 += dist_loss2.data
        total_loss += frame_loss.data

        mask2_pred = seg_net(img2)
        sg_loss = dice_loss(mask2_pred, mask2)
        dist_loss1 = contour_loss(binary_mask(mask2_pred[:, 1]), dist2[:, 1], device)
        dist_loss2 = contour_loss(binary_mask(mask2_pred[:, 1] + mask2_pred[:, 2]), dist2[:, 3], device)
        frame_loss = lambs[0] * sg_loss + lambs[1] * dist_loss1 + lambs[2] * dist_loss2
        sg_total_loss += sg_loss.data
        dist_total_loss1 += dist_loss1.data
        dist_total_loss2 += dist_loss2.data
        total_loss += frame_loss.data

        num_sample += 2
        t1 = time()
        if i % 50 == 0:
            print_loss(folder, [sg_total_loss, dist_total_loss1, dist_total_loss2, total_loss], num_sample,
                       len(loader) * 2, t1 - t0, epoch, mode='Valid')
    print_loss(folder, [sg_total_loss, dist_total_loss1, dist_total_loss2, total_loss], num_sample, len(loader) * 2,
               t1 - t0, epoch, mode='Valid')
    return sg_total_loss / (len(loader) * 2), dist_total_loss1 / (len(loader) * 2), dist_total_loss2 / (
                len(loader) * 2), total_loss / (len(loader) * 2)


#-------------------------------------------------------------------------
# Read dataset
#-------------------------------------------------------------------------
data_prefix = ''
data_folder = data_prefix + 'CAMUS_train/'

# set hyper-parameters
epochs = 100
start_epoch = 1
learning_rate = 1e-3
img_size = 256
batch = 8
lambs = [100, 0.0005, 0.0005]
fold = int(sys.argv[1])

# split train, valid, test according to the fold number
train_files, valid_files, test_files = get_train_test_files(fold, data_prefix, ch = 'all')
# Segmentation dataloader
seg_data_tr = EchoPairDataset(train_files, data_folder, imgsize=img_size, transform=data_aug)
seg_train_loader = torch.utils.data.DataLoader(seg_data_tr,
                                           batch_size=batch, shuffle=True,
                                           drop_last=True)

seg_data_valid = EchoPairDataset(valid_files, data_folder, imgsize=img_size, transform=zoom)
seg_valid_loader = torch.utils.data.DataLoader(seg_data_valid,
                                           batch_size=1, shuffle=True,
                                           drop_last=True)

seg_data_test = EchoPairDataset(test_files, data_folder, imgsize=img_size, transform=zoom)
seg_test_loader = torch.utils.data.DataLoader(seg_data_test,
                                           batch_size=1, shuffle=True,
                                           drop_last=False)

model_path = data_prefix + 'SegOnly_fold'+str(fold)+'/'
if not os.path.isdir(model_path):
    os.makedirs(model_path)
print(model_path)

#-------------------------------------------------------------------------
# Set model
#-------------------------------------------------------------------------
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
torch.autograd.set_detect_anomaly(True)

seg_model = SegNetOnly()
seg_model.to(device)

params = list(seg_model.parameters())
optimizer = torch.optim.Adam(params, lr=learning_rate)
#scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[30, 80], gamma=0.1)

loss_train = []
loss_valid = []

with open(model_path + 'train_loss.csv', 'a') as f:
    f.write("seg loss, dist_train1, dist_train2, all loss \n")
with open(model_path + 'valid_loss.csv', 'a') as f:
    f.write("seg loss, dist_train1, dist_train2, all loss \n")

with open(model_path + 'params.txt', 'a') as f:
    f.write('epochs = ' + str(epochs) +'\n')
    f.write('lr = ' + str(learning_rate) + '\n')
    f.write('cur fold = ' + str(fold) + '\n')
    f.write('imgsize = ' + str(img_size) + '\n')
    f.write('Lamdas = ' +str(lambs) + '\n')
    f.write('batchsize = ' + str(batch) + '\n')


predict_valid_limit = np.inf
sg_train = 1
predict_n = 1
for epoch in range(start_epoch, epochs+1):
    t0 = time()
    seg_model.train()
    for param in seg_model.parameters():
        param.require_grad = True
    if sg_train > 0.8:
        # if sg_train (dice loss) is higher than 0.8, then only optimize Dice loss, else, we consider contour loss
        sg_train, dist_train1, dist_train2, all_train = train_seg_model(model_path, seg_train_loader, seg_model,device, epoch, [1,0,0], optimizer, contour_flag= False)
    else:
        sg_train, dist_train1, dist_train2, all_train = train_seg_model(model_path, seg_train_loader, seg_model, device, epoch, lambs, optimizer, contour_flag=True)
    with torch.no_grad():
        seg_model.eval()
        sg_valid, dist_valid1, dist_valid2, all_valid = valid_seg_model(model_path, seg_valid_loader, seg_model, device, epoch, lambs)

    save_checkpoint({'epoch': epoch,
                     'seg_model' : seg_model.state_dict(),
                     'loss_train':[to_numpy(sg_train), to_numpy(dist_train1), to_numpy(dist_train2), to_numpy(all_train)],
                     'loss_valid':[to_numpy(sg_valid), to_numpy(dist_valid1), to_numpy(dist_valid2), to_numpy(all_valid)],
                     'optimizer': optimizer.state_dict()}, model_path)
    #scheduler.step()
    t1 = time()
    print('Time lapse for on epoch: {}'.format(t1-t0))

    with open (model_path + 'train_loss.csv','a') as f:
        f.write(str(to_numpy(sg_train)) + ',' + str(to_numpy(dist_train1)) + ',' + str(to_numpy(dist_train2)) + ',' + str(to_numpy(all_train))  + "\n")
    with open (model_path + 'valid_loss.csv','a') as f:
        f.write(str(to_numpy(sg_valid)) + ',' + str(to_numpy(dist_valid1)) + ',' + str(to_numpy(dist_valid2)) + ',' + str(to_numpy(all_valid))  + "\n")

    if all_valid< predict_valid_limit:
        predict_valid_limit = all_valid
        predict_n = epoch


#==============================================================================================
# Test split evaluation
# evaluation on test fold in the end, using the model that performed best on validation dataset
#==============================================================================================
model_name = 'model.' + str(predict_n).zfill(2) + '.pth.tar'
dict = torch.load(os.path.join(model_path, model_name))
seg_model.load_state_dict(dict['seg_model'])
num_sample = 0
t0 = time()
metric_ed = []
metric_es = []
info_list = []
with torch.no_grad():
    seg_model.eval()
    for i, sample in enumerate(seg_test_loader, 1):
        num, ch, img1, mask1, map1, _, img2, mask2, map2, _, sampling = sample
        info_list.append([num, ch])
        img1 = to_var(img1, device)
        mask1 = to_var(mask1, device)
        img2 = to_var(img2, device)
        mask2 = to_var(mask2, device)
        try:
            mask1_pred = seg_model(img1)
            mask2_pred = seg_model(img2)

            mask1_np = np.argmax(to_numpy(mask1[0]),axis=0)
            mask2_np = np.argmax(to_numpy(mask2[0]),axis=0)
            mask1_pred = np.argmax(to_numpy(mask1_pred[0]),axis=0)
            mask2_pred = np.argmax(to_numpy(mask2_pred[0]),axis=0)

            metric_ed.append(evalAllmetric(mask1_pred, mask1_np, sampling))
            metric_es.append(evalAllmetric(mask2_pred, mask2_np, sampling))
        except:
            pass

metric_ed = np.array(metric_ed)
metric_es = np.array(metric_es)
info_list = np.array(info_list)
metric_ed_csv = pd.DataFrame(metric_ed)
metric_ed_csv['Number'] = info_list[:, 0]
metric_ed_csv['CH'] = info_list[:, 1]
metric_es_csv = pd.DataFrame(metric_es)
metric_es_csv['Number'] = info_list[:, 0]
metric_es_csv['CH'] = info_list[:, 1]

metric_ed_csv.to_csv(os.path.join(model_path, str(predict_n) + 'th_predict_ed_seg.csv'))
metric_es_csv.to_csv(os.path.join(model_path, str(predict_n) + 'th_predict_es_seg.csv'))
