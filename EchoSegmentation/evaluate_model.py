'''
Evaluate the trained segmentation model on the official test part (50 patients) of CAMUS dataset
'''

import torch
import os
from dataset import *
from model import *
from utils import *
from time import time
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import SimpleITK as sitk
from skimage import transform
import pickle


def read_patient_imgs(images_path, i):
    id_pre = 'patient' + str(i).zfill(4) + '/patient' + str(i).zfill(4) + '_2CH_'
    patient_path_ed = os.path.join(images_path, id_pre) + 'ED.mhd'
    img_ed_2ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_ed))[0]
    ed2ch_shape = img_ed_2ch.shape
    patient_path_es = os.path.join(images_path, id_pre) + 'ES.mhd'
    img_es_2ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_es))[0]
    es2ch_shape = img_es_2ch.shape

    id_pre = 'patient' + str(i).zfill(4) + '/patient' + str(i).zfill(4) + '_4CH_'
    patient_path_ed = os.path.join(images_path, id_pre) + 'ED.mhd'
    img_ed_4ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_ed))[0]
    ed4ch_shape = img_ed_4ch.shape
    patient_path_es = os.path.join(images_path, id_pre) + 'ES.mhd'
    img_es_4ch = sitk.GetArrayFromImage(sitk.ReadImage(patient_path_es))[0]
    es4ch_shape = img_es_4ch.shape

    img_ed_2ch = transform.resize(img_ed_2ch, (256, 256), 1)
    img_es_2ch = transform.resize(img_es_2ch, (256, 256), 1)
    img_ed_4ch = transform.resize(img_ed_4ch, (256, 256), 1)
    img_es_4ch = transform.resize(img_es_4ch, (256, 256), 1)
    return img_ed_2ch, ed2ch_shape, img_es_2ch, es2ch_shape, img_ed_4ch, ed4ch_shape, img_es_4ch, es4ch_shape


data_prefix = ''
data_folder = data_prefix + 'testing/'

img_size = 256

predict_n_list_dict = {
    'SegOnly': [29, 26, 27, 21, 28, 27, 30, 25, 26, 25],
    'SegOnly_contour': [69, 96, 86, 68, 41, 58, 60, 59, 52, 62]
}
folder_names = ['SegOnly', 'SegOnly_contour']

m = 0
f_name = folder_names[m]
predict_n_list = predict_n_list_dict[f_name]

for f in range(10):
    fold = f+1
    model_path = data_prefix[:-5] + 'save/'+f_name+'_fold' + str(fold) + '/'
    print(model_path)
    file_save_folder = '/Results/'+ f_name + '/Fold'+str(fold) + '/'
    if not os.path.isdir(file_save_folder):
        os.makedirs(file_save_folder)

    img_save_folder = file_save_folder + 'pred_img/'
    if not os.path.isdir(img_save_folder):
        os.makedirs(img_save_folder)

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    torch.autograd.set_detect_anomaly(True)

    seg_model = SegNetOnly()
    seg_model.to(device)

    # prediction
    predict_n = predict_n_list[f]
    model_name = 'model.' + str(predict_n).zfill(2) + '.pth.tar'
    dict = torch.load(os.path.join(model_path, model_name),map_location='cpu')
    seg_model.load_state_dict(dict['seg_model'])
    num_sample = 0
    t0 = time()

    with torch.no_grad():
        seg_model.eval()
        for i in tqdm(range(1, 51)):
            img1_2ch, ed2ch_shape, img2_2ch, es2ch_shape, img1_4ch, ed4ch_shape, img2_4ch, es4ch_shape = read_patient_imgs(
                data_folder, i)

            # 2CH SEGMENTATION
            img1_2ch = torch.unsqueeze(torch.unsqueeze(to_var(img1_2ch, device),dim=0),dim=0)
            img2_2ch = torch.unsqueeze(torch.unsqueeze(to_var(img2_2ch, device),dim=0),dim=0)
            mask1_pred_2ch, _ = seg_model(img1_2ch)
            mask2_pred_2ch, _ = seg_model(img2_2ch)

            mask1_pred_2ch = np.argmax(to_numpy(mask1_pred_2ch[0]), axis=0)
            mask2_pred_2ch = np.argmax(to_numpy(mask2_pred_2ch[0]), axis=0)
            mask1_pred_2ch4 = np.rollaxis(np.eye(4, dtype=np.uint8)[mask1_pred_2ch], -1, 0)
            mask2_pred_2ch4 = np.rollaxis(np.eye(4, dtype=np.uint8)[mask2_pred_2ch], -1, 0)

            # 4CH SEGMENTATION
            img1_4ch = torch.unsqueeze(torch.unsqueeze(to_var(img1_4ch, device),dim=0),dim=0)
            img2_4ch = torch.unsqueeze(torch.unsqueeze(to_var(img2_4ch, device),dim=0),dim=0)
            mask1_pred_4ch, _ = seg_model(img1_4ch)
            mask2_pred_4ch, _ = seg_model(img2_4ch)

            mask1_pred_4ch = np.argmax(to_numpy(mask1_pred_4ch[0]), axis=0)
            mask2_pred_4ch = np.argmax(to_numpy(mask2_pred_4ch[0]), axis=0)
            mask1_pred_4ch4 = np.rollaxis(np.eye(4, dtype=np.uint8)[mask1_pred_4ch], -1, 0)
            mask2_pred_4ch4 = np.rollaxis(np.eye(4, dtype=np.uint8)[mask2_pred_4ch], -1, 0)

            with open(file_save_folder + 'patient_'+str(i).zfill(4)+'_pred.pickle', 'wb') as f:
                pickle.dump({
                    'shape_list':[ed2ch_shape, es2ch_shape, ed4ch_shape, es4ch_shape],
                    'ed2ch_pred': mask1_pred_2ch,
                    'es2ch_pred': mask2_pred_2ch,
                    'ed4ch_pred': mask1_pred_4ch,
                    'es4ch_pred': mask2_pred_4ch
                },f)

            plt.figure(figsize=(20, 10))
            plt.subplot(1, 4, 1)
            plt.imshow(img1_2ch[0, 0], cmap='gray')
            plt.imshow(update_pred_mask(mask1_pred_2ch), alpha=0.5)
            plt.title('2CH-ED_pred')

            plt.subplot(1, 4, 2)
            plt.imshow(img2_2ch[0, 0], cmap='gray')
            plt.imshow(update_pred_mask(mask2_pred_2ch), alpha=0.5)
            plt.title('2CH-ES_pred')

            plt.subplot(1, 4, 3)
            plt.imshow(img1_4ch[0, 0], cmap='gray')
            plt.imshow(update_pred_mask(mask1_pred_4ch), alpha=0.5)
            plt.title('4CH-ED_pred')

            plt.subplot(1, 4, 4)
            plt.imshow(img2_4ch[0, 0], cmap='gray')
            plt.imshow(update_pred_mask(mask2_pred_4ch), alpha=0.5)
            plt.title('4CH-ES_gt')
            plt.savefig(img_save_folder + 'patient'+ str(i).zfill(4) + '_pred.png')

